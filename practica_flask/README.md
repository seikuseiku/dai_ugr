# Cómo testear la práctica de DAI en las defensas [FLASK]

## Paso 1: Encender el servidor

En la terminal, escribir `docker-compose up`.

En el navegador, abrir `localhost:5000`.

## Paso 2: Página principal

Desde el index, partir a las siguientes páginas:

### 2.1. [Práctica 1] Dropdown de ejercicios y ejercicio 1

+ Ejercicio 2: ordena números con bubblesort.
+ Ejercicio 3: números primos.
+ Ejercicio 4: fibonacci n-ésimo.
+ Ejercicio 5: corchetes balanceados.
+ Ejercicio 6: detectar palabras.
+ Ejercicio SVG: imagen SVG aleatoria.
+ Ejercicio 1: adivinar número.



### 2.2. Usuarios

+ Registro
+ Cerrar sesión
+ Iniciar sesión



### 2.3. [Práctica 4] MongoDB: Listado de películas

+ Listado
+ Añadir película: Star Wars: Episode VII - The Force Awakens.
+ Búsqueda: Star Wars (con editar, borrar y cancelar búsqueda)



### 2.4. [Práctica 5] MongoDB: APIREST

+ **GET**

  Para listar todas las películas: `http localhost:5000/api/movies`
  Para buscar por título: `http localhost:5000/api/movies?title="Star Wars"`
  Para buscar por año: `http localhost:5000/api/movies?year="2020"`

+ **POST**
  Parar introducir una nueva película en la base de datos:`http -f POST http://localhost:5000/api/movies title="Star Wars: Episode VII - The Force Awakens" year=2015 imdb=tt2488496 type=movie`

+ **GET movie**
  Listar información de una película: `http localhost:5000/api/movies/[ID]`

+ **PUT**
  Actualizar información de una película: `http -f PUT http://localhost:5000/api/movies/[ID] title="Star Wars" year=2015 imdb=tt2488496 type=movie`

+ **DELETE**
  Borrar una película: `http DELETE localhost:5000/api/movies/[ID]`

