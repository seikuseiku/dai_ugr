from pickleshare import PickleShareDB

# ======================================================================
# Iniciar conexion a la base de datos
# ======================================================================
def get_db():
    return PickleShareDB('datos')

# ======================================================================
# Registrar usuario en la base de datos
# ======================================================================
def registrar_usuario(email, password, nombre):
    # Abrir base de datos
    db = get_db()
    # Si el usuario está registrado
    if db.get(email): 
        return False
    # Si el usuario no está registrado
    else:
        # Insertar campos
        db[email] = {'password': password, 'nombre': nombre}
        # Devolver que no hubo error
        return True

# ======================================================================
# Actualizar datos de un usuario
# ======================================================================
def actualizar_usuario(email, password, nombre):
    # Crear diccionario con los valores
    nuevo = {email: {'password': password, 'nombre': nombre}}
    # Abrir base de datos
    db = get_db()
    # Modificar campos
    db.update(nuevo)

# ======================================================================
# Devolver datos de un usuario
# ======================================================================
def get_usuario(email, all=False):
    # Abrir base de datos
    db = get_db()
    # Si el usuario está registrado, devolver diccionario con campos
    if db.get(email):
        # Con todos los campos
        if all:
            return {'email': email, 'nombre': db[email]['nombre'], 'password': db[email]['password']}
        # Solo los de sesión
        else:
            return {'email': email, 'nombre': db[email]['nombre']}
    # Si no, devolver error
    else:
        return False

# ======================================================================
# Verificar usuario con email y password
# ======================================================================
def verificar_usuario(email, password):
    # Abrir base de datos
    db = get_db()
    # Si el usuario está registrado
    if db.get(email):
        # Comprobar que la contraseña es correcta
        if db[email]['password'] == password:
            return get_usuario(email)
    
    pass
    
# ======================================================================
# Listar usuarios 
# ======================================================================
def listaremails_usuarios():
    db = get_db()
    return db.keys()

def listar_usuarios():
    db = get_db()
    mensaje = ""
    for email in db:
        mensaje += f"Email: {email} -> Password: {db[email]['password']}, Nombre: {db[email]['nombre']}.<br>"

    return mensaje