####################################################
# Todos los ejercicios de la Practica 1
####################################################
import math
import re

####################################################
# Method: BubbleSort
def bubbleSort(array):
    n = len(array)

    for i in range(n):
        for j in range(0, n-1):
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]

    return array

####################################################
# Method: Eratostenes algorithm
def SieveOfEratosthenes(n):
    # write every number 2-n
    numbers = []
    
    for i in range(1,n+1):
        numbers.append(i)

    # mark no-prime numbers
    marked = [False] * n
    bound = int(math.sqrt(n))

    for i in range(2, bound+1):
        # if i is not marked
        if marked[numbers.index(i)] == False:
            # mark every multiple of i
            for j in range(i,int(n/i)+1):
                marked[i*j-1] = True
    
    # show prime numbers
    prime_numbers = [] 

    for i in range(n):
        if (marked[i] == False):
            prime_numbers.append(numbers[i])

    return prime_numbers

####################################################
# Method: Returns the fibonacci number n
def Fibonacci(n):
    if n<=0 or n>30:
        return -1
    elif n==1:
        return 0
    elif n==2:
        return 1
    else:
        return Fibonacci(n-1)+Fibonacci(n-2)

#################################################### 
# Class: stack implementation
class Stack:
    def __init__(self):
        self.items = []

    def empty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def top(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

####################################################
# Methods: Verify couple of [] and verify string of []
def verifyCouple(a, b):
    aperture = "["
    closure = "]"
    return aperture.index(a) == closure.index(b)

def verifyString(string):
    # Create stack and some variables
    stack = Stack()
    is_balanced = True
    index = 0
    
    while index < len(string) and is_balanced:
        symbol = string[index]
        if symbol in "[":
            stack.push(symbol)
        else:
            if stack.empty():
                is_balanced = False
            else:
                pop = stack.pop()
                if not verifyCouple(pop, symbol):
                    is_balanced = False
        index = index + 1
    
    if is_balanced and stack.empty():
        return True
    else:
        return False

####################################################
# Methods: check patterns using regular expressions
def identifyWords(string):
    pattern = "\w*\s[A-Z]"
    return re.findall(pattern, string)

def identifyEmails(string):
    pattern = "[a-zA-Z0-9]+[\._]?[a-zA-Z0-9]+[@]\w+[\.]?\w+[.]\w+"
    return re.findall(pattern, string)

def identifyCreditCardNumbers(string):
    pattern = "(?:\d[ -]*?){13,16}"
    return re.findall(pattern, string)
