from flask import Flask, flash, session, request, url_for, redirect, render_template, jsonify
import re
import random, math
from pymongo import MongoClient
from bson.objectid import ObjectId

app = Flask(__name__)
app.secret_key = 'clavesecreta'

#######################################################
# Mongo
#######################################################
# Conectar al servicio (docker) "mongo" en su puerto estandar
client = MongoClient("mongo", 27017)
# Elegir la base de datos
db = client.Samples

@app.route('/editar_pelicula/<p>/<id>', methods=['GET', 'POST'])
def editar_pelicula(p, id):
    # Si se rellena el formulario
    if request.method == 'POST':
        # Actualizar los datos de la película
        db.video_movies.update({"_id": ObjectId(id)},
            {"$set": {
                "title": request.form['titulo'],
                "year": int(request.form['anio']),
                "imdb": request.form['imdb'],
                "type": request.form['tipo']
            }}
        )
        # Mensaje de edición correcto
        flash(u'Película "' + request.form.get('titulo') + '" editada correctamente.', 'success')
        # Redireccionar a la lista
        return redirect(url_for('listado_peliculas', p=p))

    # Get datos de la película
    busqueda = db.video_movies.find({"_id": ObjectId(id)}) # cursor
    datos = []
    for p in busqueda:
        datos.append(p)
    
    # Renderizar la página
    return render_template('editar_pelicula.html', datos=datos,
                                                   ult_pags=session['ult_pags'])

@app.route('/listado_peliculas')
def peliculas():
    # Borrar variable de sesión 
    session.pop('expr_busqueda', None)
    # Redireccionar a la lista
    return redirect(url_for('listado_peliculas', p=1))

@app.route('/listado_peliculas/<int:p>', methods=['GET','POST'])
def listado_peliculas(p):
    # Inicializar variable busqueda
    busqueda = ""

    # Si se rellena el formulario
    if request.method == 'POST':
        # ==================================================
        # Inserción
        # ==================================================
        if 'insercion' in request.form:
            # Insertar nueva película en la base de datos
            db.video_movies.insert({
                "title": request.form['titulo'],
                "year": int(request.form['anio']),
                "imdb": request.form['imdb'],
                "type": request.form['tipo']
            })
            # Mensaje de éxito
            flash(u'Nueva película "' + request.form['titulo'] + '" añadida correctamente.', 'success')
        
        # ==================================================
        # Búsqueda
        # ==================================================
        if 'busqueda' in request.form:
            # Realizar búsqueda
            busqueda = request.form.get('expresion')
            session['expr_busqueda'] = busqueda

        # ==================================================
        # Edición
        # ==================================================
        if 'edicion' in request.form:
            # Redireccionar a la página de edición de película
            return redirect(url_for('editar_pelicula', p=p, id=request.form['edicion']))
        
        # ==================================================
        # Borrado
        # ==================================================
        if 'borrado' in request.form:
            # Borrar documento
            titulo = db.video_movies.find_one({"_id": ObjectId(request.form['borrado'])}).get('title')
            db.video_movies.remove({"_id": ObjectId(request.form['borrado'])})
            # Mensaje de éxito
            flash(u'Película "' + titulo + '" eliminada.', 'success')
    
    # ==================================================
    # Reiterar búsqueda
    # ==================================================
    busqueda = session.get('expr_busqueda', "")

    # ==================================================
    # Renderizar lista de peliculas (con paginacion)
    # ==================================================
    # Contar número total de películas de la búsqueda
    proyeccion = {"title": re.compile(".*" + busqueda, re.IGNORECASE)}
    numero_peliculas = db.video_movies.find(proyeccion).count()

    # Número de páginas que mostrar en la barra de paginación
    PAGESIZE = 6
    MAX_PAGS = math.ceil(numero_peliculas/PAGESIZE)
    lim_inf, lim_sup = p-5, p+4 # Para mostrar 10 botones

    if lim_inf<0 and lim_sup>MAX_PAGS:
        lim_inf=0
        lim_sup=MAX_PAGS
    elif lim_inf<0:
        lim_inf=0
        lim_sup=9
    elif lim_sup>MAX_PAGS:
        lim_inf = MAX_PAGS-9
        lim_sup=MAX_PAGS

    paginas = list(range(1, math.ceil(numero_peliculas/PAGESIZE)+1))[lim_inf:lim_sup]

    # Ir a la última página si la página redireccionada no tiene entradas
    if p not in paginas and p != 0:
        p -= MAX_PAGS

    # Encontrar los documentos de la coleccion "video_movies" de esta página
    peliculas = db.video_movies.find(proyeccion).skip(PAGESIZE*(p-1)).limit(PAGESIZE)
    # Solo ordenar por título en las búsquedas (preferencia personal)
    if busqueda:
        peliculas.sort('title')
    
    # Convertir a lista
    lista_peliculas = []
    for pelicula in peliculas:
        lista_peliculas.append(pelicula)

    # Renderizar la página
    return render_template('lista.html', n=numero_peliculas,
                                         pag_actual=p,
                                         paginas=paginas,
                                         max_pag=MAX_PAGS,
                                         peliculas=lista_peliculas,
                                         busqueda=busqueda,
                                         usuario=session['nombre'],
                                         ult_pags=session['ult_pags'])

#######################################################
# APIRest
#######################################################
# Para devolver una lista (GET) o añadir (POST)
@app.route('/api/movies', methods=['GET', 'POST'])
def api_1():
    # ==================================================
    # Listado
    # ==================================================
    if request.method == 'GET':
        # Búsqueda por título
        if request.args.get('title'):
            expr = re.compile(".*" + request.args.get('title'), re.IGNORECASE)
            busqueda = {"title": expr}
        # Búsqueda por año
        elif request.args.get('year'):
            try:
                busqueda = {'year': int(request.args.get('year'))}
            except:
                return jsonify({'error': 'Incorrect arguments'}), 404
        # Búsqueda por valoracion
        elif request.args.get('valoracion'):
            try:
                busqueda = {'valoracion': int(request.args.get('valoracion'))}
            except:
                return jsonify({'error': 'Incorrect arguments'}), 404
        # Búsqueda por premio
        elif request.args.get('premio'):
            premio = request.args.get('premio')
            if premio in PREMIOS:
                try:
                    expr = re.compile(".*" + premio, re.IGNORECASE)
                    busqueda = {"premio": expr}
                except:
                    return jsonify({'error': 'Incorrect arguments'}), 404
            else:
                return jsonify({'error': 'Incorrect arguments'}), 404
        # Mostrar todo
        elif not request.args:
            busqueda = {}
        # Error
        else:
            return jsonify({'error': 'Wrong search'}), 404

        # Realizar búsqueda
        movies = db.video_movies.find(busqueda)

        # Convertir a lista
        lista = []

        for movie in movies:
            lista.append({
                'id': str(movie.get('_id')),
                'title': movie.get('title'),
                'year': movie.get('year'),
                'imdb': movie.get('imdb'),
                'type': movie.get('type'),
                'valoracion': movie.get('valoracion'),
                'premio': movie.get('premio')
            })

        # Devolver lista
        if lista:
            return jsonify(lista)
        else:
            return jsonify({'error': 'Not found'}), 404
    
    # ==================================================
    # Inserción
    # ==================================================
    if request.method == 'POST':
        # Insertar nuevos datos
        try:
            id = db.video_movies.insert({
                "title": request.form['title'],
                "year": int(request.form['year']),
                "imdb": request.form['imdb'],
                "type": request.form['type']
            })
        except:
            return jsonify({'error': 'Incorrect arguments'}), 404

        # Objeto creado para devolver datos
        movie = db.video_movies.find_one({'_id': id})

        return jsonify({
            'id': str(id),
            'title': movie.get('title'),
            'year': movie.get('year'),
            'imdb': movie.get('imdb'),
            'type': movie.get('type')
        })

    return jsonify({'error': 'Method not found'}), 404

# Para devolver una, modificar o borrar
@app.route('/api/movies/<id>', methods=['GET', 'PUT', 'DELETE'])
def api_2(id):
    # ==================================================
    # Búsqueda por id
    # ==================================================
    if request.method == 'GET':
        # Buscar y devolver objeto por id
        try:
            movie = db.video_movies.find_one({'_id': ObjectId(id)})
            return jsonify({
                'id': id,
                'title': movie.get('title'),
                'year': movie.get('year'),
                'imdb': movie.get('imdb'),
                'type': movie.get('type')
            })
        except:
            return jsonify({'error': 'Not found'}), 404

    # ==================================================
    # Edición
    # ==================================================
    if request.method == 'PUT':
        # Objeto a editar para poner parámetros por defecto
        try:
            movie = db.video_movies.find_one({'_id': ObjectId(id)})
        except:
            return jsonify({'error': 'Not found'}), 404

        # Modificar parámetros
        try:
            db.video_movies.update({"_id": ObjectId(id)},
                {"$set": {
                    "title": request.form.get('title', movie.get('title')),
                    "year": int(request.form.get('year', movie.get('year'))),
                    "imdb": request.form.get('imdb', movie.get('imdb')),
                    "type": request.form.get('type', movie.get('type'))
                }}
            )
        except:
            return jsonify({'error': 'Incorrect arguments'}), 404

        # Objeto actualizado para devolver datos
        movie = db.video_movies.find_one({'_id': ObjectId(id)})

        return jsonify({
            'id': id,
            'title': movie.get('title'),
            'year': movie.get('year'),
            'imdb': movie.get('imdb'),
            'type': movie.get('type')
        })

    # ==================================================
    # Borrado
    # ==================================================
    if request.method == 'DELETE':
        # Borrar objeto con id
        try:
            db.video_movies.remove({"_id": ObjectId(id)})
            return jsonify({'id': id})
        except:
            return jsonify({'error': 'Not found'}), 404

    return jsonify({'error': 'Method not found'}), 404


#######################################################
# Front-End con API REST
#######################################################
@app.route('/listado_peliculas_api')
def peliculas_api():
    return render_template('lista_api.html',
                            usuario=session['nombre'],
                            ult_pags=session['ult_pags'])

#######################################################
# Front-End con API REST + GRÁFICAS JAVASCRIPT
#######################################################
VALORACIONES = [1,2,3,4,5,'']
PREMIOS = ['Goya', 'PremiosBAFTA', 'PremiosCesar', 'PalmadeOro', 'DaviddeDonatello', 'OsodeOro', 'GlobodeCristal', 'GlobodeOro', '']

@app.route('/graficas')
def graficas_peliculas():
    return render_template('graficas.html',
                            usuario=session['nombre'],
                            ult_pags=session['ult_pags'])

@app.route('/graficas/generar_datos')
def generar_datos_aleatorios():
    # Recorrer películas y añadir parámetros
    movies = db.video_movies.find({})

    for movie in movies:
        try:
            db.video_movies.update({"_id": movie.get('_id')},
                {"$set": {
                    "valoracion": random.choices(population=VALORACIONES, weights=[0.1,0.1,0.15,0.2,0.2,0.25], k=1)[0],
                    "premio": random.choices(population=PREMIOS, weights=[0.1, 0.05, 0.05, 0.06, 0.08, 0.095, 0.05, 0.03, 0.485], k=1)[0]
                }}
            )
        except:
            return jsonify({'error': 'Incorrect arguments'})

    # Mensaje de datos generados correctamente
    flash(u'Datos generados correctamente.', 'success')

    # Volver a la página de gráficas
    return redirect(url_for('graficas_peliculas'))

#######################################################
# Funciones request
#######################################################
@app.before_request
def before_request():
    # Inicializar ultimas paginas si no lo estan
    if not session.get('ult_pags'):
        session['ult_pags'] = []
    # Inicializar a false el nombre para mostrar los botones correctos
    if not session.get('nombre'):
        session['nombre'] = False

    # Añadir nueva página si no es la misma que la anterior
    if not session.get('ult_pags') or session.get('ult_pags')[0] != request.path:
        # Lista de urls que no interesan
        if not request.path.startswith("/favicon") and not request.path.startswith("/static"):
            session['ult_pags'].insert(0, request.path)
            session.modified = True

            # Borrar la ultima sesión que sobra
            if len(session.get('ult_pags')) > 3:
                session['ult_pags'].pop(3)

        
#######################################################
# Modificar las variables de sesión de usuario
#######################################################
def logear_usuario(email):
    # Buscar nombre del usuario
    datos = get_usuario(email)
    # Añadir variables de sesión
    session['email'] = datos['email']
    session['nombre'] = datos['nombre']
    session['logeado'] = True
    
#######################################################
# Index
#######################################################
@app.route('/')
def index():
    # Renderizar la página
    return render_template('index.html', usuario=session["nombre"], 
                                         ult_pags=session["ult_pags"])

#######################################################
# Login
#######################################################
from model import verificar_usuario

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        # Verificar identidad del usuario
        datos = verificar_usuario(request.form['email'], request.form['password'])

        # Si la contraseña es verificada, el diccionario tiene los campos del usuario
        if bool(datos):
            # Logear usuario
            logear_usuario(datos['email'])
            # Mensaje de login correcto
            flash(u'¡Bienvenido/a, ' + session['nombre'] + '!', 'success')
            # Redireccionar al index
            return redirect(url_for('index'))
            
        # Si la contraseña no es correcta
        else:
            # Mensaje de login incorrecto
            flash(u'Email o contraseña incorrectos.', 'danger')
            
    # Mostrar el formulario de login
    return render_template('login.html')

#######################################################
# Logout
#######################################################
@app.route('/logout')
def logout():
    # Mensaje de cerrar sesión correctamente
    flash(u'¡Hasta luego, ' + session['nombre'] + '!', 'info')
    # Eliminar datos de la sesión
    session.pop('email', None)
    session['nombre'] = False
    session['logeado'] = False
    # Redireccionar al index de la página
    return redirect(url_for('index'))

#######################################################
# Registro
#######################################################
from model import registrar_usuario

@app.route('/registro', methods=['GET', 'POST'])
def registro():
    if request.method == 'POST':
        # Si las contraseñas de la doble confirmación coinciden
        if request.form['password1'] == request.form['password2']:
            # Registrar usuario en la base de datos
            exito = registrar_usuario(email=request.form['email'],
                                    password=request.form['password1'],
                                    nombre=request.form['nombre'])
            # Si se ha podido guardar en la base de datos
            if exito:
                # Logear usuario
                logear_usuario(request.form['email'])
                # Mensaje de login correcto
                flash(u'Se ha registrado correctamente. ¡Bienvenido/a, ' + session['nombre'] + '!', 'success')
                # Redireccionar al index
                return redirect(url_for('index'))
            # Si no se ha podido registrar
            else:
                # Mensaje de registro incorrecto
                flash(u'El email que ha usado ya está registrado.', 'danger')
                # Mostrar otra vez formulario de registro
                return render_template('registro.html')
        else:
            # Mensaje de formulario incorrecto
            flash(u'Las contraseñas no coinciden.', 'danger')
            # Mostrar formulario de registro
            return render_template('registro.html')
    else:
        # Mostrar formulario de registro
        return render_template('registro.html')

#######################################################
# Perfil de usuario
#######################################################
from model import get_usuario

@app.route('/perfil')
def perfil():
    # Si el usuario está logeado
    if session['logeado']:
        # Buscar datos del usuario
        datos = get_usuario(session['email'], all=True)
        # Mostrar informacion de usuario
        return render_template('perfil.html', email=datos['email'],
                                              nombre=datos['nombre'],
                                              usuario=session['nombre'],
                                              ult_pags=session["ult_pags"])
    # Si no
    else:
        # Mostrar error
        flash(u'No está logueado.', 'info')
        # Redireccionar al index de la página
        return redirect(url_for('index'))

#######################################################
# Modificar usuario
#######################################################
from model import get_usuario, actualizar_usuario

@app.route('/editar_usuario', methods=['GET', 'POST'])
def editar_usuario():
    if request.method == 'POST':
        # Actualizar datos de usuario
        actualizar_usuario(email=request.form['email'],
                           password=request.form['password'], 
                           nombre=request.form['nombre'])
        # Logear usuario
        logear_usuario(request.form['email'])
        # Mensaje de edición correcto
        flash(u'Se han editado los datos correctamente.', 'success')
        # Redireccionar al perfil del usuario
        return redirect(url_for('perfil'))

    else:
        # Si el usuario está logeado
        if session['logeado']:
            # Mostrar informacion de usuario
            datos = get_usuario(email=session['email'], all=True)
            return render_template('editar_usuario.html', email=datos['email'],
                                                          password=datos['password'],
                                                          nombre=datos['nombre'],
                                                          usuario=session['nombre'],
                                                          ult_pags=session["ult_pags"])
        # Si no hay usuario logeado
        else:
            # Mostrar error
            flash(u'No está logueado.', 'info')
            # Redireccionar al index de la página
            return redirect(url_for('index'))

#######################################################
# EJERCICIO 1
# Adivinar número (con pistas)
#######################################################
random_number = random.randint(1,100)
n_intento = 1

@app.route('/ejercicio1')
def ejercicio1():
    # Enunciado del ejercicio
    mensaje = "¿Podrás adivinar el número?"
    # Resetear contador
    global n_intento
    n_intento = 1
    # Renderizar template con la información
    return render_template('ejercicio1.html', mensaje=mensaje, 
                                              restart=False, 
                                              ult_pags=session['ult_pags'])

@app.route('/ejercicio1', methods=['POST'])
def ejercicio1_post():
    # Recuperar variables
    global random_number
    global n_intento
    restart = True

    if n_intento in range(1,10):
        guess = int(request.form['guess'])
        mensaje = ""

        # Si lo ha adivinado
        if guess == random_number:
            mensaje = f"¡Buen trabajo! El número era {random_number} y lo has adivinado en {n_intento} intentos."
            # Reseteo de variables globales
            n_intento = 1
            random_number = random.randint(1,100)
            restart = True

        # Si no lo ha adivinado
        else:
            # Si el usuario aun tiene intentos
            if random_number > guess:
                mensaje = f"¡Intentalo de nuevo! El número es mayor que {guess}. (Te quedan {10-n_intento} intentos)."
            else:
                mensaje = f"¡Intentalo de nuevo! El número es menor que {guess}. (Te quedan {10-n_intento} intentos)."

            n_intento+=1
            restart = False
    
    else:
        mensaje = f"Mala suerte, se te han acabado los intentos. El número era {random_number}."
        restart = True

    # Renderizar template con la información
    return render_template('ejercicio1.html', mensaje=mensaje, 
                                              restart=restart, 
                                              ult_pags=session['ult_pags'])

#######################################################
# EJERCICIO 2
# Ordenar vector de números usando Bubblesort
#######################################################
from ejerciciosp1 import bubbleSort

@app.route('/ejercicio2/<values>')
def ejercicio2(values):
    # Lista (strings) de valores a ordenar
    m = re.findall(r'(\d+|-\d+)', values)
    # Ordenar valores
    valores_ordenados = bubbleSort(m)

    # Enunciado del problema
    enunciado = "Ordenar un vector de números usando el algoritmo Bubblesort."
    # Información sobre la solución
    mensaje = "Los valores ordenados del vector (%s) son:" % values
    # Solución: string con el vector (de strings) ordenado
    solucion = ', '.join(valores_ordenados)

    # Renderizar template con la información
    return render_template('ejercicio.html', numero=2, 
                                             enunciado=enunciado, 
                                             mensaje=mensaje, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# EJERCICIO 3
# Listar los números primos hasta n usando
# la Criba de Eratóstenes
#######################################################
from ejerciciosp1 import SieveOfEratosthenes

@app.route('/ejercicio3/<int:value>')
def ejercicio3(value):
    # Obtener números primos
    result = SieveOfEratosthenes(value)

    # Enunciado del problema
    enunciado = "Obtener los números primos menores que un número natural dado usando el algoritmo de la Criba de Eratóstenes."
    # Información sobre la solución
    mensaje = "Los números primos hasta %d son:" % value
    # Solución: string con el vector (de ints) de números primos
    solucion = ', '.join(map(str, result))

    # Renderizar template con la solución
    return render_template('ejercicio.html', numero=3, 
                                             enunciado=enunciado, 
                                             mensaje=mensaje, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# EJERCICIO 4
# Número n-ésimo de la función de Fibonacci
#######################################################
from ejerciciosp1 import Fibonacci

@app.route('/ejercicio4/<int:value>')
def ejercicio4(value):
    # Obtener fibonacci(n)
    fibo = Fibonacci(value)

    # Enunciado del problema
    enunciado = "Obtener el valor n-ésimo de la sucesión de Fibonacci."
    
    # Error en los parámetros 
    if fibo == -1:
        mensaje = "Por favor, introduzca un número entre 0 y 30."
        solucion = ""
    else:
        # Información sobre la solución
        mensaje = f"El número {value}-ésimo de la sucesión de Fibonacci es:"
        # Solución: n-ésimo en la sucesión de Fibonacci
        solucion = f"{fibo}"

    # Renderizar template con la solución
    return render_template('ejercicio.html', numero=4, 
                                             enunciado=enunciado, 
                                             mensaje=mensaje, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# EJERCICIO 5
# Balanceo de corchetes usando una pila
#######################################################
from ejerciciosp1 import verifyString

@app.route('/ejercicio5/<s>')
def ejercicio5(s):
    # Enunciado del problema
    enunciado = "Comprobar que una cadena de corchetes está balanceada."
    # Información sobre la solución
    mensaje = f"Ha introducido la cadena {s}."
    # Solución: ver si es balanceada o no
    if verifyString(s): solucion = f"{s} es una cadena balanceada."
    else: solucion = f"{s} no es una cadena balanceada."

    # Renderizar template con la solución
    return render_template('ejercicio.html', numero=5, 
                                             enunciado=enunciado, 
                                             mensaje=mensaje, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# EJERCICIO 6
# Expresiones regulares para identificar:
#   - palabras seguidas de un espacio y una letra mayúscula
#   - emails válidos
#   - números de tarjetas de crédito separados por guiones o espacios en blanco
#######################################################
from ejerciciosp1 import identifyWords, identifyEmails, identifyCreditCardNumbers
@app.route('/ejercicio6/<value>')
def ejercicio6(value):
    # Mirar si es un nombre, email o número de tarjeta de crédito correcto o incorrecto
    r1 = identifyWords(value)
    r2 = identifyEmails(value)
    r3 = identifyCreditCardNumbers(value)
    
    # Enunciado del problema
    enunciado = "Identificar palabras seguidas de espacio y una letra mayúscula, direcciones email válidas y números de tarjeta cuyos dígitos estén separados por guiones o por espacios en blanco en paquetes de cuatro dígitos."
    # Información sobre la solución
    mensaje = f"Ha introducido {value}."
    # Solución: ver si es un nombre, email o número de tarjeta de crédito válido
    solucion = ""
    if r1: solucion = solucion + "Se ha encontrado el nombre: " + ', '.join(r1) + "<br>"
    if r2: solucion = solucion + "Se ha encontrado el email: " + ', '.join(r2) + "<br>"
    if r3: solucion = solucion + "Se ha encontrado número de tarjeta de crédito: " + ', '.join(r3) + "\n"

    # Renderizar template con la solución
    return render_template('ejercicio.html', numero=6, 
                                             enunciado=enunciado, 
                                             mensaje=mensaje, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# Para nota: crear imagen SVG dinamica
#######################################################
@app.route('/svg')
def svg():
    # Elegir aleatorios
    formas = ["circle", "rect", "ellipse", "mountain", "star"]
    forma = random.choice(formas)

    # Solución: imagen SVG
    solucion =  '<div><svg version="1.1" xmlns="http://www.w3.org/2000/svg" ' 
    solucion += 'width="100%" height="450">'

    # Si es círculo
    if forma is "circle":
        r = random.randint(40,60)
        cx = cy = r*2+5
        solucion += '<circle cx="' + str(cx) + '" cy="' + str(cy) + '" r="' + str(r) + '" '
    
    # Si es rectángulo
    elif forma is "rect":
        width = random.randint(40,150)
        height = random.randint(40,150)
        solucion += '<rect width="' + str(width) + '" height=" ' + str(height) + '"'

    # Si es una estrella
    elif forma is "star":
        solucion += '<polygon points="100,10 40,198 190,78 10,78 160,198" '
    
    # Si es elipse
    elif forma is "ellipse":
        rx = random.randint(30,60)
        ry = random.randint(30,60)
        cx = rx*2+5
        cy = ry*2+5
        solucion += '<ellipse cx="' + str(cx) + '" cy="' + str(cy) + '" rx="' + str(rx) + '" ry="' + str(ry) + '" '

    # Si es polígono
    elif forma is "mountain":
        solucion += '<polygon points="10,70 20,40 40,100 70,10 100,100 120,40 130,70" '

    # Colores y transformación común para todos
    colores = ["orange", "yellow", "RoyalBlue", "Peru", "pink", "black"]
    fill = random.choice(colores)
    stroke = random.choice(colores)
    stroke_width = random.randint(0,5)
    tx = random.randint(0,500)
    ty = random.randint(0,300)

    solucion += 'fill="' + str(fill) + '" '
    solucion += 'stroke="' + stroke + '" stroke-width="' + str(stroke_width) + '" '
    solucion += 'transform="translate(' + str(tx) + ',' + str(ty) + ')"'
    solucion += '/></div>'

    # Enunciado del problema
    enunciado = "Mostrar imagen SVG aleatoria."

    # Renderizar template con la solución
    return render_template('ejercicio.html', numero="SVG", 
                                             enunciado=enunciado, 
                                             solucion=solucion,
                                             ult_pags=session["ult_pags"])

#######################################################
# Page not found
#######################################################
@app.errorhandler(404)
def page_not_found(error):
    return "Página no encontrada, inténtelo otra vez.", 404