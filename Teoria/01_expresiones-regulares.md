# Expresiones regulares

Las expresiones regulares son un lenguaje para especificar búsquedas en un texto mediante patrones.

En python se usan con el método `re`:

```python
import re
match = re.search(r'([\w.-]+)@([\w.-]+)', str)
```

## Métodos

- `re.match()`: determinar si la expresión regular se ajusta al principio de la cadena.
- `re.search()`: buscar en toda la cadena dónde se cumple la expresión regular.
- `re.findall()`: buscar en la cadena dónde se cumple la expresión regular y devolver una lista.
- `re.finditer()`: buscar en la cadena dónde se cumple la expresión regular y devolver un iterador.

## Metacarácteres

En una expresión regular cada carácter significa él mismo, excepto los comodines:

- **Alternancia `|` y agrupado `()`**

```python
'granada|o'     # granada ó o
'granad(a|o)'   # granada o granado
```

- **Conjunto `[]`**

```python
'granad[ao]'    # granada o granado
'granad[^ao]'   # conjunto negado
'granad[a-z]'   # intervalo
```

Conjuntos frecuentes:

```python
.      # cualquier menos \n
\d     # dígito: [0-9]
\D     # no dígito: [^0-9]
\w     # alfanumérico: [a-zA-Z0-9_]
\W     # no alfanumérico: [^a-zA-Z0-9_]
\s     # espacio: [ \t\f\r\n]
\S     # no espacio: [^ \t\f\r\n]
```

- **Conjuntos unicode**

```python
\p{L}      # cualquer letra
\p{Lu}     # máyusculas
\p{Ll}     # minúsculas
\p{Lt}     # mayúsculas principio de palabra
\p{Z}      # separador
\p{S}      # símbolo
\p{P}      # puntuación
\p{Latin}
\p{Arabic}
```

- **Cuantificadores `?`, `*`, `+`, `{}`**

```python
'uno?'       # cero o una vez: un, uno
'uno*'       # cero o más: un, uno, unoo, ...
'uno+'       # uno o más: uno, unoo, unoo, ...
'uno{3}'     # tres: unooo
'uno{3,5}'   # entre tres y cinco
```

- **Modificador `?` para cuantificadores**

Las expresiones regulares abren todo lo posible. Para que no abran se modifican con `?`.

```python
# Detectar las etiquetas en una página web
'<.+?>'
```

- **Anclajes `^`, `$`, `\b`**

```python
'^uno'        # uno al principio del string
'uno$'        # uno al final del string
'\buno\b'     # frontera de palabra
```

- **Extracción con `()`**

Se puede extraer el texto de parte de una expresión regular capturándolo con paréntesis `()`.

```python
# Segunda palabra de un texto
import re
segunda = re.findall(r'^\w+ (\w+)', texto)
```

- **Referencias hacia atrás**

También sirven para referenciar parte de la expresión regular dentro de ella misma con `\1`, `\2`, ... .

```python
# Dos primeras palabras iguales
import re
re.search (r'^(\w+) \1', texto)
```

- **Paréntesis con nombre**

También nombrando cada paréntesis para referirse a él con el método `group()`.

```python
import re
m = re.search('(?P<dia>\d\d)-(?P<mes>\d\d)-(?P<año>\d\d)', texto)
                          # Si no tienen nombre sería
dia = m.group('dia')      # m.group(1)
mes = m.group('mes')      # m.group(2)
año = m.group('año')      # m.group(3)
```

- **Asociaciones hacia delante**

Son condiciones que se tienen que cumplir para que la expresión regular se cumpla, aunque no formen parte de ella (no se consumen).

- `(?=...)` positiva
- `(?!...)` negativa

```python
# Ficheros con extensión distinta de .bat
import re
m = re.match(r'.+\.(?!bat)$', texto)
```

- **Aserciones hacia atrás**

También hacia atrás con:

- `(?<=...)` positiva
- `(?<!...)` negativa

Las aserciones no forman parte de la expresión regular.

```python
# Palabras en máyuscula después de punto
import re
m = re.match(r'(?<=\.)\s+[A-Z][a-z]+', texto)
```

## Modificadores

Se puede modificar el comportamiento de las expresiones regulares con los modificadores:

- `DOTALL`, `S`: el punto `.` incluye todos los caracteres, incluyendo el salto de línea.
- `IGNORECASE`, `I`: no se distingue mayúsculas de minúsculas.
- `MULTILINE`, `M`: `^` y `$` son principio y fin de línea, en lugar de principio y fin de cadena.
- `VERBOSE`, `X`: se ignoran espacios en blanco, saltos de línea y comentarios, para hacer la expresión regular más legible.

Los modificadores se ponen en el tercer argumento de los métodos:

```python
m = search( r'''  \d+  # parte entera
                  \.   # separador
                  \d+  # parte decimal
             ''', str, re.X)
if m: print(m.group())
```

## Modificación de cadenas

- `re.split()`: partir la cadena por donde se cumpla la expresión regular.
- `re.sub()`: sustituir las partes de la cadena donde se cumpla la expresión regular.
- `re.subn()`: devuelve además el número de sustituciones hechas.

```python
import re
# Partir un texto en palabras
palabras = re.split(r'\s+', texto)
# Substituir espacios por guiones
sub = re.sub(r'\s+', '-', texto)
```

## Compilación de expresiones regulares

Si se van a usar varias veces, las expresiones regulares se pueden compilar:

```python
import re
vocales = re.compile(r'[aeiou]', re.I)
# Secuencias de consonantes
b = vocales.split(str)
```

## Métodos de string

Si se puede, es más rápido usar los métodos de **string**:

- `find()`: devuelve la posición de una subcadena.
- `replace()`: devuelve una cadena con sustituciones.
- `translate()`: devuelve una cadena con caracteres sustituidos.
- `split()`: devuelve una lista con partes de una cadena.
- `strip()`: devuelve la cadena sin espacios en blanco en los extremos.

```python
str = ' abc def '
str.find('abc')
str.replace ('abc', '123')
str.translate(' ','-')
str.split('d')
str.strip()
```