# Persistencia: Bases de datos

## CRUD

![img](imagenes/crud-rails-1-768x406.png)



## Opciones para la Persistencia de Datos

- Sistema de archivos
- Persistencia de datos básica
- Archivos indexados
- Bases de datos relacionales (SQL)
- Bases de datos NoSQL
- Motores de búsqueda



## Serialización de datos

Con el módulo **pickle**, guardamos un objeto en el disco:

```python
import pickle
D = {'a':1, 'b':2}  # o cualquier estructura de datos

with open('d.dat', 'wb') as f:
	pickle.dump(D, f) # serializa (convierte a un flujo de bytes)

with open('d.dat', 'rb') as f
	D = pickle.load(f) # deserializa

print(D['a'])
```

>  [pickle — Python object serialization](http://docs.python.org/3.7/library/pickle.html)



## Archivos indexados tipo `dbm`

Con **dbm** se accede bases de datos tipo `Berkely DB` como si estuvieran en memoria:

```python
import dbm
db = dbm.open('datos.dat', 'c')

# Insert (en disco)
db['237483J'] = 'Pepito Pérez|c/ Pedro Antonio|10|7º B'

# Select (desde el disco)
datos = db['237483J'].split('|')  # una lista

db.close()
```

> [dbm — Interfaces to Unix “databases”](http://docs.python.org/3.7/library/dbm.html)



## Persistencia de objetos **`shelve`**

**shelve** añade una capa de software a `dbm` para serializar los objetos:

```python
import shelve

db = shelve.open('datos.dat')

# Insert (en disco)
db['237483J'] = {'nombre': 'Pepito Pérez',
                 'dir':'c/ Pedro Antonio, 10, 7º B'}

# Select (desde el disco)
datos = db['237483J']

db.close() # flush
```

>  [shelve - Python object persistence](https://docs.python.org/3.7/library/shelve.html)



## Persistencia de objetos **`pickleshare`**

Las aplicaciones web se suelen ejecutar en paralelo en varias hebras, **pickleshare** añade concurrencia a `shelve`:

```python
from pickleshare import PickleShareDB
db = PickleShareDB('~/datos.dat')

# Insert (en disco)
db['237483J'] = {'nombre': 'Pepito Pérez',
                 'dir':'c/ Pedro Antonio, 10, 7º B'}

# Select (desde el disco)
datos = db['237483J']
```

>  [PickleShare](https://pypi.org/project/pickleshare/)



## **B**ases de **D**atos

Usar bases de datos:

```python
# Con clientes de BD (distintos para cada DB)
result = connection.execute("select username from users")
for row in result:
    print("username: %s" % row['username'])

# Con ORMs (código igual para cualquier BD)
result = connection.query(users).all()
for user in result:
   print("username: %s" % user.username)
```



## Bases de datos [NoSQL](http://en.wikipedia.org/wiki/NoSQL)

Se hicieron para tratar con [Big Data](https://youtu.be/bAyrObl7TYE?t=34), para controlar el volumen, la velocidad y la variedad de datos. Pueden tratar con tipos de datos complejos, más índices, distintas maneras de consulta, etc.

>  [Definición de big data](https://www.oracle.com/es/big-data/what-is-big-data.html) 
>
> [An Introduction To NoSQL Databases](https://www.youtube.com/watch?v=uD3p_rZPBUQ) 
>
> [What is NoSQL?](http://www.mongodb.com/nosql)

| **SQL**                                                      | **NoSQL**                                                  |
| ------------------------------------------------------------ | ---------------------------------------------------------- |
| Tablas                                                       | Hashes, listas                                             |
| Datos estructurados, consistencia                            | Datos menos estructurados, dinamicidad                     |
| Escalabilidad vertical                                       | Escalabilidad horizontal                                   |
| Contabilidad, Facturación, Almacen, Personal, Tiendas on-line, etc | Documentos, Foros, Redes Sociales, Datos Distribuidos, etc |

> [Choosing Database SQL vs NoSQL](https://www.youtube.com/watch?v=R5adYE8s0Zs)



### Tipos de **NoSQL** DB

![img](imagenes/387725-179-635691101939114107_338x600_thumb.jpg)

>  [Types of NoSQL Databases](https://www.youtube.com/watch?v=p4C0n3afZdk)



### Base de datos [mongoDB](https://docs.mongodb.com/guides/)

Es una base de datos, orientada a documentos estilo [JSON](https://en.wikipedia.org/wiki/JSON), ([BSON](https://es.wikipedia.org/wiki/BSON)) que incluye información del tipo de dato. 
En **mongodb**, una [base de datos ](http://docs.mongodb.org/manual/reference/glossary/#term-database)está formada por [colecciones](http://docs.mongodb.org/manual/reference/glossary/#term-collection) (equivalentes a tablas en SQL), que son un conjunto de [documentos](http://docs.mongodb.org/manual/core/document/) (equivalentes a filas).



> [MongoDB CRUD Introduction](http://docs.mongodb.org/manual/core/crud-introduction/)

- [Read Operations](http://docs.mongodb.org/manual/core/read-operations/)
- [Insert Operations](https://docs.mongodb.com/manual/tutorial/insert-documents/)
- [Update Operations](https://docs.mongodb.com/manual/tutorial/update-documents/)
- [Delete Operations](https://docs.mongodb.com/manual/tutorial/remove-documents/)

>  [Getting Started with MongoDB (MongoDB Shell Edition)](https://docs.mongodb.com/getting-started/shell/)



#### Collections

Los documentos son similares a objetos diccionario de python.

![img](imagenes/index.png)

Cada documento incluye un campo **_id**, similar una llave primaria en SQL.

![img](imagenes/1_65P8gE1JkCGgZDHWhVt3gg.png)



#### Librería [PyMongo](https://pymongo.readthedocs.io/en/stable/tutorial.html)

```python
from pymongo import MongoClient
client = MongoClient('mongodb://localhost:27017/')

# base de datos
db = client['test-database']

# documento
import datetime
post = {"author": "Mike",
        "text": "My first blog post!",  # en SQL serian 3 tablas
        "tags": ["mongodb", "python", "pymongo"],
        "date": datetime.datetime.utcnow()}

# colección
posts = db.posts
# insert_one()
post_id = posts.insert_one(post).inserted_id # devuelve _id

# Query
query = posts.find_one({"_id": post_id})
query = posts.find_one({"author": "Mike"})

# Insertar otros datos (distintos)
new_posts = [{"author": "Mike",
              "text": "Another post!",
              "tags": ["bulk", "insert"],
              "date": datetime.datetime(2009, 11, 12, 11, 14)},
             {"author": "Eliot",
              "title": "MongoDB is fun",
              "text": "and pretty easy too!",
              "date": datetime.datetime(2009, 11, 10, 10, 45)}]
posts.insert_many(new_posts)


# Find devuelve un cursor iterable
for post in posts.find():
   print(post['text'])

# Otras querys
d = datetime.datetime(2009, 11, 12, 12)
posts.find({"date": {"$lt": d}}).sort("author")

# Creando índices para mayor rapidez (y tamaño de la BD)
from pymongo import ASCENDING, DESCENDING
posts.create_index([("date", DESCENDING), ("author", ASCENDING)])
```

> [Más Ejemplos](http://api.mongodb.org/python/current/examples/index.html)
>
> [Tutorial](http://api.mongodb.org/python/current/tutorial.html)
>
> [Otrotutorial](https://www.w3schools.com/python/python_mongodb_getstarted.asp)
>
> [Un vídeo](https://www.youtube.com/watch?v=rE_bJl2GAY8)



#### También

Un Object Document Mapper para mongodb: [mongoengine](http://mongoengine.org/#home)



### Otras BD NOSQL muy usadas

- [Redis](http://redis.io/) (para caché en memoria, paso de mensajes, etc)

- [Elasticsearch](https://www.elastic.co/es/what-is/elasticsearch) (para motores de búsqueda)

- [Hadoop](http://hadoop.apache.org/) (big data)

- [Firebase](https://www.google.com/search?client=firefox-b-d&q=firebase+database&sa=X&ved=2ahUKEwi5nfCVg-bsAhWLZMAKHebPDMwQ1QIoAnoECBEQAw&biw=1536&bih=750) (back end para móviles) 

