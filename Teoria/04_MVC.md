# MVC: Modelo-Vista-Controlador

Consiste en separar en archivos y carpetas distintas (módulos, packages) el código relativo a:

+ Base de datos (model)
+ Interfaz de usuario (view)
+ Comunicaciones y lógica (controler)

[MVC architecture](https://developer.mozilla.org/en-US/docs/Glossary/MVC)

![](imagenes/mvc.png)

[MVC explained in 4 minutes](https://www.youtube.com/watch?v=DUg2SWWK18I)



### Modelo

Interacción con la base de datos (SELECT, INSERT, UPDATE, DELETE).

Lógica relacionada con los datos (cuanta más mejor), p.e. restricciones a los datos, campos derivados, etc.

Se comunica con el controlador.

### View

Es donde interacciona el usuario.

Usualmente HTML, CSS y JavaScript.

Se comunica con el controlador.

A través de plantillas (templates).

### Controler

Recibe la entrada de las vistas o urls (routing).

Procesa los requests (GET, POST, PUT, DELETE).

Interacciona con el modelo.

Rellena y envía las plantillas.

![](/imagenes/mvc2.jpg)

[Qué es MVC y por que es tan usado en el desarrollo Web](https://yosoy.dev/mvc-y-su-importancia-en-la-web/)

### Organizándose

```
mi_app/
  ├── app.py
  ├── templates/           # plantillas html
  |    ├── base.html
  |    ├── entrada.html
  │    └── ...
  └── static/              # archivos fijos (assets)
      ├──css/
      ├──imagenes/
      └──js/
```

```
mi_app/
  ├── run.py  
  ├── mi_app/              # código
  │    ├── model.py
  │    └── controler.py
  ├── templates/           # plantillas html (vistas)
  |    ├── base.html
  │    ├── entrada.html
  │    └── ...
  └── static/              # archivos fijos (assets)
       ├──css/
       ├──imagenes/
       └──js/
```

### Ficheros

```python
# model.py (con ORM)
from django.db import models

class Musician(models.Model):
    first_name = models.CharField(max_length=50, unique=True)
    last_name = models.CharField(max_length=50, required=False)
    instrument = models.CharField(max_length=100)

class Album(models.Model):
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    release_date = models.DateField(default=datetime.date.today)
    num_stars = models.IntegerField()
```

```python
# controller.py (consultas)
todos = Musician.objects.all()
uno = Musician.objects.get(first_name="Pepe")
pianistas = Musician.objects.filter(instrument='piano').order_by('first_name')
garcia = Musician.objects.fiter(last_name__contains='Garcia')
albumes_de_pepe = Album.objects.filter(artist=uno)
```

```html
<!-- view.html (con plantillas) -->
<p> My string: {{my_string}} </p>
<p> Value from the list: {{Users[2]}} </p>
<p> Loop through the list: </p>
<ul>
  {% for user in Users %}
      <li> {{user.name}} </li>
  {% endfor %}
</ul>
```

[Jinja](https://jinja.palletsprojects.com/en/2.11.x/)

