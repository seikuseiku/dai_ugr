



# XML a fondo

## e**X**tensible **M**arkup **L**anguage

Es un metalenguaje de propósito general para intercambio y almacenamiento de información.

Es un subcojunto simplificado del [SGML](https://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language), diseñado para que sea legible también por personas.

[XML](http://en.wikipedia.org/wiki/XML)



![img](imagenes/sgml-xml-relationships.png)



[SGML-XML](http://cscie12.dce.harvard.edu/lecture_notes/2007-08/20080130/slide26.html)



## Uso del XML

Tiene un uso muy amplio, sobre todo en internet:

| `XHTML``RSS``Standarized Vector Graphic``MathML``Open Document Format` | `Office Open Format``Web Services Description Language``Simple Object Access Protocol``XML-RPC`... |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                                              |                                                              |

## Niveles de XML

Hay dos niveles de restricción en los archivos XML:



| `**Válidos**`       | Cumplen con la sintáxis del XML                              |
| ------------------- | ------------------------------------------------------------ |
| ` `                 |                                                              |
| `**Bien formados**` | Que además tengan una especificación propia para sus elementos (que se puedan validar con un `DTD` o un `XML Schema`) |

## XML bien formado

En un archivo XML bien formado nos podemos encontrar:

| PrólogoElementosAtributosReferencias a caracteres | Entidades predefinidasComentariosSecciones CDATA (character data)Instrucciones de procesamiento |
| ------------------------------------------------- | ------------------------------------------------------------ |
|                                                   |                                                              |

## Prólogo

Un documento XML comienza por una etiqueta inicial y quizás con una declaración de un DTD o esquema.





```
<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE xxx  PUBLIC "http://...">
```



UTF-8 es la codificación por defecto, si se usa otra hay que ponerla.



## Elementos

Delimitados por etiquetas de principio y fin, excepto si son vacios, en cuyo caso solo contienen la etiqueta con autocierre  `<.../>`

Los elementos se estructuran en un árbol.

Siempre hay un elemento raíz que incluye a todos los demás:



```
<raiz>
   <elemento_1> texto </elemento_1>
   <elemento_2> texto </elemento_2>
   <elemento-vacio/>
</raiz>
```



## Atributos

Los atributos van en la etiqueta de apertura. Es obligatorio que tengan un valor entre comillas (simples o dobles).





```
<root>
   <elemento_1 atributo="valor"> texto </elemento_1>
   <elemento_2> texto </elemento_2>
   <elemento-vacio/>
</root>
```



## Referencias a entidades carácter

En XML se puede poner cualquier signo en la codificación que se use, normalmente UTF-8, o con su código numérico UNICODE, o con referencia a *entidades predefinidas*:



```
&#NNNN;   Notación decimal

&#xNNNN;  Notación hexadecimal

&referencia_a_entidad_caracter;
```

[Character Entities Referece chart](http://dev.w3.org/html5/html-author/charref)



## Caracteres reservados

En XML están reservados los caracteres:



```
         &   →   &amp;
         <   →   &lt;
         >   →   &gt;
         "   →   &quot;
         '   →   &apos;
```



## Comentarios,  secciones CDATA

Comentarios:



```
<!-- Comentario -->
```

Las secciones CDATA sirven para incluir texto que el parser no va a procesar:



```
<![CDATA[
 ...
]]>
```



## Instrucciones de procesamiento



Se utilizan para poner indicaciones para las aplicaciones que procesen el archivo XML.

La sintaxis es:  `<?target instruction ?>`.



```
<?xml-stylesheet href="es.css" type="text/css">
```


p.e. se pueden aplicar hojas de estilo a documentos xml



## Reglas para XMLs bien formados

- Siempre tiene que haber un solo elemento raiz.
- Los elementos no vacios tienen etiqueta de inicio y de fin:
  `<x> ... </x>`
- Elementos vacios con marca de autocierrre: `<x/>`
- Los elementos no se pueden cruzar:
  `<a> <b>...</b> </a> o <a>...</a> <b>...</b>`
- El valor de los atributos se delimita con comillas.
- [Diferencias entre HTML y XHTML](http://es.wikipedia.org/wiki/XHTML#Diferencias_entre_HTML_y_XHTML)

## DTDs y XML Schemmas

Un ` **DTD** ` (Document Type Definition) o un ` **XML Schemma**  ` son declaraciónes de la sintaxis para un lenguaje XML particular, con las definiciones de los elementos y los atributos.



| Ejemplo de [DTD](http://www.w3schools.com/xml/xml_dtd.asp). | Ejemplo de [Schemma](http://www.w3schools.com/xml/xml_schema.asp). |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
|                                                             |                                                              |





Para que un documento XML sea **válido**, tiene que tener una referencia a un DTD o a un Schemma.



## Espacios de Nombres





A veces puede haber interferencia cuando se usa en un mismo documento distintos lenguajes XML. Para distinguir los elementos de cada uno se usan los **Espacios de nombres**.

[Espacio de nombres XML](http://es.wikipedia.org/wiki/Espacio_de_nombres_XML)



## Procesado de archivos XML

Hay 3 maneras (como mínimo) de procesar archivos XML (o HTML):

- Usando ` **expresiones regulares**`
- Con parsers ` **[SAX](https://en.wikipedia.org/wiki/Simple_API_for_XML)**`
- Con parsers ` **[DOM](https://en.wikipedia.org/wiki/Document_Object_Model)**`



Las librerías para SAX y DOM son estándar y existen con las mismas funciones para C, C++, PHP, Java, Perl, etc.



## Expresiones regulares

```
import re

# 'portada.xml' es un rss en
# http://ep00.epimg.net/rss/elpais/portada.xml

file = open('portada.xml', encoding='utf-8')
contenido = file.read()
file.close()

parser = re.compile('<.+?>|[^<>]*', re.S)

for e in parser.findall(contenido):
	print("[%s]" % e.replace ('\n', '\\n'))

	if e.find('</') >= 0:
		print("etiqueta final")

	elif e.find('<') >= 0:
		print("etiqueta inicial")
	else:
		print("texto")
```



## Biblioteca de eventos: **SAX**





**S**imple **A**pi para **X**ml es una libreria basada en eventos: se va leyendo el archivo y se generan llamadas a funciones conforme se leen las etiquetas.

[Processing Large XML Wikipedia Dumps](https://www.youtube.com/watch?v=AeRN4zI7Dhk)



## ax parser

```
# etree sax parser
from lxml import etree

class ParseRssNews():           # manejadores de eventos

	def __init__(self):
		print('--------------------- Principio del archivo')

	def start(self, tag, attrib):          # Etiquetas de inicio
		print('<%s>' % tag)
		for k in attrib:
			print('%s = "%s"' % (k,attrib[k]))

	def end(self, tag):                    # Etiquetas de fin
		print('</%s>' % tag)

	def data(self, data):                  # texto
		print('-%s-' % data)

	def close(self):
		print('---------------------- Fin del archivo')


parser = etree.XMLParser(target=ParseRssNews())
url = 'http://ep00.epimg.net/rss/elpais/portada.xml'
etree.parse(url, parser)
```

[Parsing XML and HTML with lxml](http://lxml.de/parsing.html)



## Manejo dinámico del árbol: **DOM**





**D**ocument **O**bject **M**odel es una API para manejar la estructura de árbol de un documento XML: se lee todo el archivo en memoria y se accede a cada nodo del árbol para leerlo, modificarlo, ampliarlo, etc.

[What is the Document Object Model?](https://developer.mozilla.org/es/docs/Referencia_DOM_de_Gecko/Introducción)



## Árbol DOM



![img](imagenes/dom1.png)





![img](C:\Users\ana\Desktop\dom2.png)

## DOM Parser

```
import xml.dom.minidom

# parse file into a dom
file = open('portada.xml', encoding='utf-8')
dom = xml.dom.minidom.parse(file)
file.close()

lista_items = dom.getElementsByTagName('item')

for item in lista_items:
    item.setAttribute('modificado', 'hoy')
    otro_elemento = dom.createElement('otro')
    texto = dom.createTextNode('Un texto que ponemos en el elemento')
    otro_elemento.appendChild(texto)
    item.appendChild(otro_elemento)

print(dom.toxml())
```

[xml.dom.minidom ](http://docs.python.org/3.3/library/xml.dom.minidom.html)  [JavaScript HTML DOM](http://www.w3schools.com/js/js_htmldom.asp)





## lxml etree parser

El módulo `lxml` tiene un API más sencillo y adaptado a python que el DOM usual: el **etree**

```
from lxml import etree
file = open ('portada.xml', encoding='utf-8')
tree = etree.parse(file)
file.close()

rss = tree.getroot() # elemento raiz

# Los elementos funcionan como listas
channel = rss[0]     # Primer hijo

for e in  channel:
	if (e.tag == 'item'):  # Atributo tag, nobre del elemento
		# Añado 
		e.set('modificado', 'hoy')

		# Los atributos del xml funcionan casi como diccionarios
		print (e.keys(), e.get('modificado'))

		# Creo otro elemento
		otro = etree.Element('otro')   # 

		# Atributo text
		otro.text = 'Texto de otro'
		e.insert(0, otro) # Añado Texto de otro (posicion inicial)

print(etree.tounicode(rss, pretty_print=True))
```

[The lxml.etree Tutorial](http://lxml.de/tutorial.html)



## lxml etree iterparser

Ejemplo de como recorrer un archivo con `lxml etree iterparser` (en lugar de SAX):



```
# etree iterparse

from lxml import etree

context = etree.iterparse ('http://ep00.epimg.net/rss/elpais/portada.xml',
                            events=('start','end'))

for action, element in context:
	text = ""

	if (element.text != None):
		text = element.text.encode('utf-8')

	print ('%s - %s - %s' % (action, element.tag, text))
```

[iterparse and iterwalk](http://lxml.de/parsing.html#iterparse-and-iterwalk)



## XPATH

`XPATH` es un minilenguaje para especificar partes de un árbol DOM:

```
from lxml import etree

tree = etree.parse('http://ep00.epimg.net/rss/elpais/portada.xml')

items = tree.xpath('/rss/channel/item')

for i in items:
	categorys = i.xpath('category')
	for c in categorys:
		print(c.text)      # Textos en category

for url in tree.xpath('//@url'):
	print(url)                 # Todos los atributos url

for t in tree.xpath('//text()'):
	print(t)                   # Todos los textos
```

[XPath: XML Path language](http://www.mclibre.org/consultar/xml/lecciones/xml_xpath.html), 

## Soporte para HTML

`lxml`  también tiene soporte para archivos `html`.



```
from lxml import etree

parser = etree.HTMLParser()
tree = etree.parse('http://www.aemet.es/es/portada', parser)

# Todos los enlaces
urls=tree.xpath('//@href')

for u in urls:
	print(u)
```

[Parsing html](http://lxml.de/parsing.html#parsing-html)





