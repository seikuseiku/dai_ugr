# HTML5, CSS frameworks

## Versiones HTML

- **HTML 2.0, (1995)**  en los primeros navegadores
- **HTML 3.2, (1997)**   ya incluyó formularios y tablas
- **HTML 4.0, (1997)**   con CSS
- **HTML 4.01, (1999)**  el que venimos usando
- **XHTML 1.0, (2000)**  
- **. . .**   plugins: flash player, java, javascript, silverlight, actionscript
- **HTML5, (2014)**   versión actual

[HTML](https://en.wikipedia.org/wiki/HTML)



## Novedades del HTML5

- Nuevo parsing, no basado en ` SGML`,  se dispara con ` <!DOCTYPE html>`.
- Elementos multimedia  `<audio>, <video> y <canvas>`.
- Elementos *semánticos* para substituir a ` <div> ` y ` <span> `: ` <section> `, ` <article> `, ` <header> `, ` <footer> `, ` <nav> `.
- Nuevos elementos para forms: ` <datalist> `, tipos de entrada para campos: `text, date, url, email, tel, number, color, etc ` con validación del navegador.

- Inclusión de [`SVG`](https://www.w3schools.com/graphics/svg_intro.asp) y [`MATHML`](https://www.w3.org/Math/whatIsMathML.html)
- Soporte de [geolocalización](https://www.w3schools.com/html/html5_geolocation.asp)
- Soporte para [arrastrar y soltar](https://www.w3schools.com/html/html5_draganddrop.asp)
- Soporte de almacenamiento local:  [Web storage](https://codeburst.io/using-html5-web-storage-a450294484bb)
- HTML5  [Canvas](https://www.w3schools.com/Html/html5_canvas.asp)
- Soporte para CSS3

[HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)

Sin embargo, **HTML5** todavía no está completamente implementado y no todos los navegadores soportan todas las propuestas.

[Test HTML5](http://html5test.com/)



## Novedades del CSS3

+ [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
+ [Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
+ Permite [responsive design](http://www.w3schools.com/css/css_rwd_intro.asp)

[Novedades CSS3](https://desarrolloweb.com/manuales/css3.html)



## CSS Frameworks

A la hora del diseño de una página web ayudarían plantillas para:

- **elementos complejos** que podamos poner, como menús, enlaces, forms.
- **situar** los elementos en la página, que queden legibles e iguales en todos los navegadores.
- **que cambie** adecuadamente con el tamaño del distpositivo: móvil, tablet o PC.

- tamaño del distpositivo: móvil, tablet o PC

[11 Best CSS Frameworks To Look Forward In 2020](https://dzone.com/articles/11-best-css-frameworks-to-look-forward-in-2020)



## Twitter Bootstrap

Es una colección de plantillas de css y js originalmente usada en Twitter para:

- Responsive design (adaptación automática a móviles y tablets).
- Grid System (posicionar los elementos en la página).
- Interface UI (forms, botones, menús, etc).

Además se pueden encontrar [recursos](https://startbootstrap.com/bootstrap-resources/) aparte de la distribución oficial [Bootstrap](http://getbootstrap.com/).



## Usando bootstrap

1. Descargamos el código desde [Bootstrap](http://getbootstrap.com/docs/4.0/getting-started/download/) o usamos un [Content Delivery Network](http://getbootstrap.com/docs/4.0/getting-started/download/#bootstrapcdn).
2. Nos bajamos alguna de las plantillas de los [Ejemplos](https://getbootstrap.com/docs/4.1/examples/).
3. Nos fijamos en el [sistema de grid](https://getbootstrap.com/docs/4.1/examples/grid/), para controlar como aparecerán los elementos en distintos tamaños de pantalla.
4.  Vamos añadiendo componentes tal como viene en la [documentación](https://getbootstrap.com/docs/4.1/components/alerts/).

[Bootstrap tutorial](Bootstrap tutorial)



## Otras alternativas: Materialize

[Materialize](https://materializecss.com/)

[Google Material](https://material.io/)

[Design](https://en.wikipedia.org/wiki/Material_Design)