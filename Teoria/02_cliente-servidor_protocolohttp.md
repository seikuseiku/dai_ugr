# Cliente-Servidor, Protocolo HTTP

Las aplicaciones web están distribuidas en dos partes que se ejecutan en máquinas distintas:

+ **Cliente**: Proactivo, inicia la comunicación. Hace requerimientos a un servidor.
+ **Servidor**: Reactivo, está esperando requerimientos para responderlos. Responde a varios clientes.

![](/imagenes/frontendbackend.png)

![](/imagenes/frontendbackend2.png)

[Cliente-Servidor](http://infomotions.com/musings/waves/clientservercomputing.html)

![](/imagenes/frontendbackend3.png)

La comunicación puede ser [síncrona o asíncrona](https://docs.dataabstract.com/Introduction/WorkingSynchronouslyOrAsynchronously/).

![](/imagenes/frontendbackend4.png)

Hay varios tipos de clientes antendidos por un solo servidor.

![](/imagenes/frontendbackend5.png)

### Ventajas

+ El cliente se ejecuta en un navegador o como aplicación nativa (más raramente).
+ Por tanto, es independiente de la plataforma. Vale para cualquier SO, incluyendo móviles.
+ Se adaptan fácilmente a los cambios en los dispositivos de acceso a la red.
+ Es fácil hacer actualizaciones.
+ Se puede usar desde cualquier sitio remotamente.
+ Se pueden poner como [Software as a Service](https://en.wikipedia.org/wiki/Software_as_a_service) con subscripciones.

### HTTP: Hiper Text Transfer Protocol

Para comunicar clientes y servidores se usa HyperText Transfer Protocol, un protocolo [sin estado](https://es.wikipedia.org/wiki/Protocolo_sin_estado) (por unas [razones](https://www.youtube.com/watch?v=L610Z5X-QTw)). Es un protocolo cliente-servidor para comunicar aplicaciones. 

[Generalidades del protocolo HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Overview).

Es un protocolo ASCII con peticiones (requests) y respuestas (responses).

+ **Request**: [Request Headers más importantes](https://platzi.com/tutoriales/1638-api-rest/4333-http-request-headers-mas-importantes/).

  ![](/imagenes/frontendbackend6.png)

  Está asociado a las operaciones del [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete).

  [Ultimate Guide to 9 Common HTTP Methods](https://nordicapis.com/ultimate-guide-to-all-9-standard-http-methods/).

  ![](/imagenes/frontendbackend7.png)

  Con GET los parámetros de la consulta van en la [URL](https://es.wikipedia.org/wiki/Localizador_de_recursos_uniforme) [URLificados](https://www.w3schools.com/tags/ref_urlencode.asp) (HTTP en ASCII).

  ![](/imagenes/frontendbackend8.png)

  Cuando no es GET, los parámetros van detrás de las cabeceras:

  ```
  POST /recepcion_formulario HTTP/1.1
  Host: ...
  ...
  variable_1 = valor_variable_1
  variable_2 = valor_variable_2
  ```

  que se correspondería con:

  ```html
  <form action="recepcion_formulario" method="post">
      <input name="variable_1">
      <input name="variable_2">
  </form>
  ```

+ **Response**: [HTTP status codes](https://restfulapi.net/http-status-codes/)

  ![](/imagenes/frontendbackend9.png)

  

