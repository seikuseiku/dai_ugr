# Formularios

## Formularios

Django incluye una clase [Forms](https://docs.djangoproject.com/en/2.2/ref/forms/api/) para facilitar la entrada de datos desde [formularios html](https://developer.mozilla.org/es/docs/Learn/HTML/Forms/Your_first_HTML_form)

![img](./imagenes/form-sketch-low.jpg)

[Tutorial](https://tutorial.djangogirls.org/es/django_forms/)



La clase incluye:

- Generación del html en las plantillas
- Validación en el servidor
- Tratamiento de los errores
- Seguridad CSRF

[Working with forms](https://docs.djangoproject.com/en/3.1/topics/forms/)



## Por ejemplo

Queremos hacer:

![](C:\Users\ana\Downloads\uni\DAI\Teoria\imagenes\Captura de pantalla 2020-11-30 113619.png)

```
<form action="/formulario/" method="post">
   <label for="tu_nombre">Tu nombre:</label>  
   <input id="tu_nombre" type="text" name="tu_nombre"
          palceholder="Tu nombre">
   <input type="submit" value="OK">
</form>
```

```
# en forms.py
   from django import forms

   class NameForm(forms.Form):
       tu_nombre = forms.CharField(label='Tu nombre',max_length=100)
```

```
   # en formulario.html (templates)
   <form action="/formulario/" method="post">
      {% csrf_token %}  # token de seguridad
      {{ form }}
      <input type="submit" value="Submit" />
   </form>
```

[Forms fields](https://docs.djangoproject.com/es/3.1/ref/forms/fields/)

```
# en views.py
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import NameForm

# viene del url /formulario/
def get_name(request):
    form = NameForm(request.POST)

    if request.method == 'POST':
        # creamos una instancia con los datos del request

        if form.is_valid():                # validamos
            # procesamos los datos de form.cleaned_data
            # nombre = form.cleaned_data.get('tu_nombre')
	    ...
            return HttpResponseRedirect('/gracias/')  
    else:
    # enviamos el formlario limpio,
    # o con errorres si is_valid() == 'False'
    return render(request, 'name.html', {'form': form})
```

## Validaciones

Están previstas validaciones automáticas en el servidor:

```
from django import forms
from django.core.validators import RegexValidator

class NameForm(forms.Form):
   tu_nombre = forms.CharField (
   label='Tu nombre',
   max_length=100,
   ...
   validators=[
      RegexValidator(r'^\D+$',
                     message="Tu nombre no debe incluir dígitos"
                    )
              ]
                               )
	
```

[Built-in validators](https://docs.djangoproject.com/es/3.1/ref/validators/#built-in-validators)

## Derivando forms de models

Incluso

```
from django.forms import ModelForm
from .models import Musician
class MusicianForm(ModelForm):
	class Meta:
		model = Musician
		fields = ['name', 'last_name']

form = MusicianForm()
if form.is_valid():  	# Directamente
	form.save()	# Toda la info puede estar en el model
			# validaciones, restricciones, mensajes, etc

   
```

[Creating forms from models](https://docs.djangoproject.com/en/3.1/topics/forms/modelforms/)

## Personalizando los forms

[Widgets](https://docs.djangoproject.com/en/3.1/ref/forms/widgets/), info para el html

[How to Render Django Form Manually](https://simpleisbetterthancomplex.com/article/2017/08/19/how-to-render-django-form-manually.html)
[Advanced Form Rendering with Django Crispy Forms](https://simpleisbetterthancomplex.com/tutorial/2018/11/28/advanced-form-rendering-with-django-crispy-forms.html)

