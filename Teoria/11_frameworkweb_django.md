# Frameworks Web: Django

## Framework **Django**

Es el framework para Python [más usado](http://www.bestwebframeworks.com/web-frameworks-usage/#python). Empezó a desarrollarse como herramienta para [sitios de prensa](http://www2.ljworld.com/) en 2008. Es software libre y está enfocado a sitios basados en bases de datos (p.e. [ebay](http://www.ebay.es/))

> [Django](https://www.djangoproject.com/)



## Arquitectura MVT

- \- **M**odel: Interface con la BD (mediante un ORM)
- \- **V**iew  El programa (lo equivalente a *Controler* en **MVC**)
- \- **T**emplate

![img](./imagenes/mvt.png)

> [Django MVT](http://www.maestrosdelweb.com/images/2012/04/esquema-mtv.png)



## Módulos

Django include:

+ Object Relational Mapper
+ Interface de administración de la BD
+ Autentificación
+ Autorización
+ Sesiones
+ Seguridad
+ Signals
+ Templates
+ Formularios
+ Messages
+ Caché
+ Localización e internacionalización
+ e-mail.

> [Django Documentation](https://docs.djangoproject.com/en/3.1/contents/),  [Django Packages](https://djangopackages.org/packages/p/django-plugins/),  [Django at glance](https://docs.djangoproject.com/en/3.1/intro/overview/)



## Flask  vs  Django

| **Flask**                                                    | **Django**                                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `Microframework (Librería), 2MB`                             | `Framework (Librería+scripts), 7MB`                          |
| `Se hace menos código para empezar, respuesta más rápida`    | `Más código hecho (usuarios, interface de adminstracion, ORM, etc)` |
| `Prototipos, sitios sin usuarios, optimización del rendimiento` | `Modelo de usuarios, autentificación, roles, middleware , etc` |
| [`Pirates use Flask, the Navy uses Django`](https://wakatime.com/blog/14-pirates-use-flask-the-navy-uses-django) | [`"Aunque" (comparativa de velocidad básica)`](http://kracekumar.com/post/117948332935/simple-json-response-basic-test-between-flask-and) |

| **Flask**                                                    |
| ------------------------------------------------------------ |
| ` from flask import render_template` <br />@app.route('/hello/<username>') def hello(): return render_template('hello.html', name=username) ` |
| **Django**                                                   |
| ` from django.shortcuts import render def hello(request, username): context = { 	'name': username, } return render(request, 'hello.html', context)` |



**Django urls.py**

```
# En el archivo urls.py
# se ponen todos los path agrupados
# ---------------------------------

from django.urls import path
from . import views

urlpatterns = [
	path('/hello/<username>/', views.hello, name='hello'),
	...
]


# El argumento name es para referirse a la ruta
# desde los templates con {% url 'hello' %}
```

## Flujo de datos



![img](imagenes/basic-django.png)



## Middleware

- El middleware:

- 
- Intercepta el **request**
- para añadir y información
- y
- conforma el **response**





![img](./imagenes/middleware.png)

[Django Middleware](https://docs.djangoproject.com/en/3.1/topics/http/middleware/)



## Middleware



![img](imagenes/a5a84a55f20f23415b1df000871de9e3.png.jpeg)
En su orden



## Pasos para crear una aplicación

1. Crear un proyecto
2. Crear una aplicación dentro del proyecto
3. Poner la base de datos (SQL) y demás en `settings.py`
4. Añadir los módulos y middleware en `settings.py`
5. Crear un superusuario
6. Definir el modelo en `app/models.py`
7. Crear la base de datos (migración)
8. Crear los mappings para los urls en `urls.py`
9. Definir las vistas en `app/views.py` y los templates
10. Aplicar tests
11. Desplegar la aplicación en el servidor web de producción

> [Tutorial](https://docs.djangoproject.com/en/3.1/intro/tutorial01/)



## Tutoriales

> [Tutorial de Django Girls](https://tutorial.djangogirls.org/es/)
>
> [Get Started With Django](https://realpython.com/get-started-with-django-1/)
>
> [Why I love Python (and Django)?](https://medium.com/@cvarelaruiz/why-i-love-python-and-django-26596ce4d82e)

