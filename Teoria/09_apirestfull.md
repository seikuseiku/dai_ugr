# API RESTfull



## Servicios Web

Son un conjunto de protocolos y estandares para comunicar aplicaciones y intercambiar datos en internet

Se usa http y el puerto 80 para evitar problemas con los firewall

La interface (**API**) puede ser:

- **REST**, más simple.
- o **SOAP**, **WSDL** de nivel más alt, cada vez menos usados.

[Servicios Web](http://es.wikipedia.org/wiki/Servicio_web),  [What Are APIs? - Simply Explained](https://www.youtube.com/watch?v=OVvTv9Hy91Q)



## **S**imple **O**bject **A**cccess **P**rotocol

Es un protocolo estandarizado para el intercambio de mensajes estructurados 

- Se usaba para transacciones complejas que requieran alta seguridad, como [pasarelas de pago](https://developer.paypal.com/docs/classic/api/PayPalSOAPAPIArchitecture/#), [ERP](https://es.wikipedia.org/wiki/Sistema_de_planificación_de_recursos_empresariales)s, etc
- Solo intercambia XML
- Puede usar varios protocolos: HTTP, FTP, SMTP, etc
- Puede ser con estado, e intercambio confiable de información

[Soap vs Rest](https://octoperf.com/blog/2018/03/26/soap-vs-rest)



## **RE**presentational **S**tate **T**ransfer

Muy generalizado para acceso a servicios:

- [Google Maps](https://developers.google.com/maps/documentation/javascript/tutorial)
- [telegram](https://core.telegram.org/)
- [twitter](https://developer.twitter.com/en/docs), [facebook](https://developers.facebook.com/docs/apis-and-sdks?locale=es_ES)
- [Google](https://developers.google.com/apis-explorer/?hl=es), [AWS](https://www.sumologic.com/insight/aws/)
- [y muchos más](https://www.programmableweb.com/apis/directory)
- y **cualquier back end para móvil**

Este término se usa ahora para referirse a interfaces simples, que solo usan ` HTTP ` y ` JSON o XML ` en la respuesta

Se basan en una sintaxis universal para acceder a los recursos en internet:  las  [**U**niform **R**esource **I**dentifier](https://es.wikipedia.org/wiki/Transferencia_de_Estado_Representacional)

En las ` URIs ` se usa [ Código tanto por ciento](http://en.wikipedia.org/wiki/Percent-encoding)

[REST](http://en.wikipedia.org/wiki/Representational_State_Transfer)

Es un protocolo  **sin estado**

La llamada (usualmente **GET**):

```
http://servidor/.../funcion?par1=valor1&par2=valor2
```

que se hace desde un navegador, una app de móvil, o desde un servidor y la respuesta es ` XML`, ` JSON ` o ` HTML`

[XML](http://www.w3schools.com/xml/default.asp)  [JSON](http://www.w3schools.com/js/js_json_intro.asp)



Para hacer llamadas desde un servidor se puede usar la librería **request**

```
import request
r = requests.get("http://www.google.es/search?q=dai")

# lo mismo
r = requests.get("http://www.google.es/", params={'q':'dai'})

# la respuesta
print (r.text)
```

[request](https://requests.kennethreitz.org/en/master/)

Desde consola con [curl](https://www.keycdn.com/support/popular-curl-examples) o con [httpie](https://httpie.io/)

```
> curl https://www.keycdn.com

> curl -X POST http://www.yourwebsite.com/login/ 
          -d 'username=yourusername&password=yourpassword'

# httpie
> http -f POST httpbin.org/post hello=World
```



## RESTfull

En su versión mas extendida, se usan los [verbos de HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods) para las llamadas del cliente al servidor, asociándolos a operaciones [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete).

Aquí lo cuentan: [RESTful Web services: The basics](http://www.ibm.com/developerworks/library/ws-restful/index.html)



Un servicio **REST**full, debe seguir 4 principios:

- Usar los verbos de HTTP explícitamente
- Ser completamente sin estado
- Usar URIs estilo path
- Devolver XML o JSON

[RESTful Web services: The basics](http://www.ibm.com/developerworks/library/ws-restful/index.html)



**Usar los verbos de HTTP explícitamente** con llamadas AJAX:

```
GET para requerir un recurso (Read,  SELECT)

POST para crear un recurso (Write, INSERT)

PUT para cambiar el estado de  un recurso (Write, UPDATE)

DELETE para borrar un recurso (DELETE)
```

[RESTful Web services: The basics](http://www.ibm.com/developerworks/library/ws-restful/index.html)



**Usar los verbos de HTTP explícitamente**

```
# en lugar de:
GET /add_restaurants?name=Bar Pepe

# usamos:
POST /restaurants 
Host: myserver
...
name=Bar Pepe
```

[Using HTTP Methods (GET, POST, PUT, etc.) in Web API](https://exceptionnotfound.net/using-http-methods-correctly-in-asp-net-web-api/)

```
# en lugar de:
GET /delete_restaurant?name=Bar Pepe

# usamos:
DELETE /restaurants
Host: myserver
...
name=Bar Pepe
```

[Using HTTP Methods (GET, POST, PUT, etc.) in Web API](https://exceptionnotfound.net/using-http-methods-correctly-in-asp-net-web-api/)



**Ser completamente sin estado**,para que el resultado de la llamada no dependa de las anteriores, (sea **idempotente** y cacheable).

```
# Con estado:
GET /resultados?pag=pagina_siguente

# Sin estado:
GET /resultados?pag=2
```

[Statelessness in REST APIs](https://restfulapi.net/statelessness/)

**Usar URIs estilo path**, (**clean uris**)

```
# en lugar de:
GET /data_restaurant?name=Bar Pepe

# mejor:
GET /restaurants/Bar-Pepe
```

[7 Rules for REST API URI Design](https://blog.restcase.com/7-rules-for-rest-api-uri-design/)

**Usar URIs estilo path**,
Que se puedan leer por humanos (SEO) y que muestren una jerquia, substituyendo espacios en blanco por guiones o subrayados

```
GET /restaurants/Bar%20Pepe/cousine/

GET /restaurants/Bar-Pepe/address/
```

[Posicionamiento en buscadores](https://es.wikipedia.org/wiki/Posicionamiento_en_buscadores)



Resumen: [¿Qué son las APIs y para qué sirven?](https://www.youtube.com/watch?v=u2Ms34GE14U)

Código: [Python REST API Tutorial - Building a Flask REST API](https://www.youtube.com/watch?v=GMppyAPbLYk)