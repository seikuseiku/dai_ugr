# Modelos en Django

## Model

Django utiliza un **ORM** para Bases de Datos **SQL**



![img](./imagenes/Django-Models.png)



[Django overview](https://docs.djangoproject.com/en/3.1/intro/overview/)



## Clases en lugar de tablas



```
from django.db import models

class Musician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name  = models.CharField(max_length=50)
    instrument = models.CharField(max_length=100)

class Album(models.Model):
    artist       = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name         = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars    = models.IntegerField()
```

[Fields types](https://docs.djangoproject.com/es/3.1/ref/models/fields/#field-types)



## Django models



Una clase Model tiene:

- Declaración de campos
- Métodos
- Metadatos



[Django Models](https://docs.djangoproject.com/en/3.1/topics/db/models/)



## Fields



```
from django.db import models

	class Musician(models.Model):
	first_name   = models.CharField(max_length=50, primary_key=True)
	last_name    = models.CharField(max_length=50, blank=True)
	instrument   = models.CharField(
		default   = 'piano',
		choices   = ('piano', 'guitar', 'flute'),
		help_text = "Seleccione el instrumento")
		)
	age = models.IntegerField(validators=[MaxValueValidator(99)])
	
```

[Fields options](https://docs.djangoproject.com/en/3.1/ref/models/fields/#field-options), [Validators](https://docs.djangoproject.com/en/3.1/ref/validators)



## Llave primaria automática



Por defecto, django asigna una llave primaría automática



```
id = models.AutoField(primary_key=True)
   
```


A no ser que se asigne un campo como 'primary_key' o como 'unique'

[Automatic primary key fields](https://docs.djangoproject.com/es/3.1/topics/db/models/#automatic-primary-key-fields)



## Relacciones


**Muchos a uno**



```
from django.db import models

   class Manufacturer(models.Model):
      # ...

   class Car(models.Model):
      manufacturer = models.ForeignKey(Manufacturer,
                                       on_delete=models.CASCADE)
      # ...
   
```

[Many-to-one relationships](https://docs.djangoproject.com/es/3.1/topics/db/models/#many-to-one-relationships)



**Muchos a muchos**



```
from django.db import models

   class Author(models.Model):
      # ...

   class Books(models.Model):
      # ...
      author = models.ManyToManyField(Author)
   
```

[Many-to-Many relationships](https://docs.djangoproject.com/es/3.1/topics/db/models/#many-to-many-relationships)



**Uno a uno**



```
from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)

	bio = models.TextField(max_length=500, blank=True)
```

[One-to-One relationships](https://docs.djangoproject.com/es/3.1/topics/db/models/#one-to-one-relationships)



## Opciones meta



```
# model.py
class Musician(models.Model)

# ...

   class Meta:
      ordering = ['name', '-instrument']
		verbose_name = 'Músicos'
		indexes = [
			models.Index(fields=['last_name', 'first_name']),
		   	  ]

   
```

[Meta options](https://docs.djangoproject.com/es/3.1/topics/db/models/#meta-options)



## Métodos

```
   class Person(models.Model):
      first_name = models.CharField(max_length=50)
      last_name = models.CharField(max_length=50)
      birth_date = models.DateField()

      def baby_boomer_status(self):
         "Returns the person's baby-boomer status."
         import datetime
         if self.birth_date < datetime.date(1945, 8, 1):
            return "Pre-boomer"
         elif self.birth_date < datetime.date(1965, 1, 1):
            return "Baby boomer"
         else:
            return "Post-boomer"

      @property
      def full_name(self):
         "Returns the person's full name."
         return '%s %s' % (self.first_name, self.last_name)
   
```

[Model methods](https://docs.djangoproject.com/es/3.1/topics/db/models/#model-methods)



## Sobrecarcagando los métodos por defecto

```
class Musician(models.Model):

   # ...

   def save(self, *args, **kwargs):
      do_something()
      super().save(*args, **kwargs)  # Call the "real" save() method.
      do_something_else()
   
```

[Overriding predefined model methods](https://docs.djangoproject.com/es/3.1/topics/db/models/#overriding-predefined-model-methods)



## admin.py

Para que aparezan en la app de admin de la BD



```
# app/admin.py

from django.contrib import admin
from .models import Musician, Album

admin.site.register('Musician')
admin.site.register('Album')
```

[Escribiendo su primera aplicación Django, parte 7](https://docs.djangoproject.com/es/3.1/intro/tutorial07/#writing-your-first-django-app-part-7)



## CREATE



```
from .models import Musician

# creando registros
b = Musician(first_name='John', last_name='Lenon')
b.save()

# Lo cambio
b.name = 'Juan'
b.save()   # si no no se graba
```

[Creating objects](https://docs.djangoproject.com/en/3.1/topics/db/queries/#creating-objects)



## Queries

```
todos = Musician.objects.all()


juanes = Muscian.objects.filter(name="Juan")


# Los tres primeros juanes ordenados
juanes = Muscian.objects.filter(name__startswith="Juan").order_by('last_name')[0:3]


# uno solo
lenon = Musician.objects.get(name='John', last_name='Lenon')
```

[Retrieving objects](https://docs.djangoproject.com/en/3.1/topics/db/queries/#retrieving-objects)



## Usando BD NO-SQL

Con [mongoengine](http://mongoengine.org/), que está muy inspirado en el ORM de django

```
from mongoengine import *
connect('encuestas', host='localhost', port=27017)

class Choice(EmbeddedDocument):
   choice_text = StringField(max_length=200)
   votes       = IntField(default=0)

class Poll(Document):
   question = StringField(max_length=200)
   pub_date = DateTimeField(help_text='date published')
   choices  = ListField(EmbeddedDocumentField(Choice))
   
```

[Django with mongodb](https://staltz.com/djangoconfi-mongoengine/#/)



