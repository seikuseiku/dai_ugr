# Plantillas (templates)

Flask usa `Jinja2` como motor de plantillas:

+ HTML escaping
+ Filtros
+ Herencia de plantillas
+ Compilado a python nativo eficiente
+ Sintaxis configurable (para acomodarse mejor a otros formatos de salida como LaTeX o JavaScript)



### Hola mundo con plantillas

```python
# holamundo.py
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
	return render_template('hola.html')
```

```html
<!-- hola.html -->
<html>
    <head>
        <title>¡Hola mundo! con plantilla</title>
    </head>
    <body>
        <p>¡Hola mundo! (con plantilla)</p>
    </body>
</html>
```

### Paso de variables

[Paso de variables](https://jinja.palletsprojects.com/en/master/templates/#variables).

```python
@app.route('/user/')
@app.route('/user/<user>')
def hello_world(user=None):
    return render_template('hola.html', usuario=user)
```

```html
{% if usuario %}
	<h1>Bienvenido {{usuario}}!</h1>
{% else %}
	<h1>Bienvenido desconocido!</h1>
{% endif %}
```

### Filtros

Efectuan modificaciones en el html. [Built in Filters](https://jinja.palletsprojects.com/en/master/templates/#list-of-builtin-filters).

```html
{{ 42.55|round }}
-> 43.0
{{ 42.55|round(1, 'floor') }}
-> 42.5
{{ 42.55|round|int }}
-> 43
{{ 43221|filesizeformat }}
-> 43 KB
```

### Bucles

[Control structures](https://jinja.palletsprojects.com/en/master/templates/#list-of-control-structures).

```python
def hello_world():
    tabla = ['primero', 'segundo', 'tercero']
    return render_template('hola.html', rows=tabla)
```

```html
<ul>
    {% for row in rows %}
    	<li>{{row}}</li>
    {% endfor %}
</ul>
```

### Acceso a diccionarios

```python
def hello_world():
    tabla = [{'a': 1, 'b':2}, {'a': 7, 'b':8}]
    return render_template('hola.html', rows=tabla)
```

```html
<ul>
    {% for row in rows %}
        <li>{{row.a}}, {{row.b}}</li>
    {% endfor %}
</ul>
```

### Sintaxis *moustache*

[Synopsis](https://jinja.palletsprojects.com/en/master/templates/#synopsis).

**{% ... %}** 	*Sentencias*.

**{{  ...   }}**  	*Expresiones* que serán impresas en la salida de la plantilla.

**{#  ...  #}**  	*Comentarios* (no aparecerán en la salida).

**#   ...**     	*Sentencias de una línea*:

### Herencia de plantillas

Permite crear una base, la cual luego será personalizada en otras plantillas [Template inheritance](https://jinja.palletsprojects.com/en/2.11.x/templates/#template-inheritance).

![](/imagenes/template-inheritance.png)

```html
<!-- base.html -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="style.css"/>
        <title>{% block title %} Mi sitio  web {% endblock %}</title>
    </head>
    <body>
        <div id="content">
            {% block content %}
            {% endblock %}
         </div>
        <div id="footer">
            {% block footer %}
            © Copyright 2008 by <a href="http://domain.invalid/">LALALA</a>.
            {% endblock %}
        </div>
    </body>
</html>
```

```html
<!-- hijo.html -->
{% extends "base.html" %}

{% block title %} Otra página {% endblock %}

{% block content %}
    <h1>Título de la página</h1>
    <p class="important">
      ¡Bienvenido piltrafilla!
    </p>
{% endblock %}
```

### Otros enlaces

 [Flask Tutorial #2 - HTML Templates](https://www.youtube.com/watch?v=xIgPMguqyws)

[Flask Tutorial #3 - Adding Bootstrap and Template Inheritance](https://www.youtube.com/watch?v=4nzI4RKwb5I)

