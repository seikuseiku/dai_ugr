# Codificación de caracteres: Unicode

## Codificaciones para signos

En Europa Occidental usamos ahora (2016), 4 codificaciones distintas para los signos del alfabeto latino:

- **ASCII**
- **ISO-8859-1**,  ISO-8859-15,  "Latin1" (ANSI en Windows)
- **UTF-8**
- **UTF-16** (UNICODE en Windows)



## ASCII

**A**merican  **S**tandard  **C**ode for  **I**nformation  **I**nterchange es un código de **7 bits** creado en 1963, solo para los signos que se usan en inglés.

Nota sobre [el fin de línea](http://es.wikipedia.org/wiki/Salto_de_línea), se pone como:

- `\n  ` (unix)
- `\r\n ` (windows)
- `\r ` (mac)

[ASCII](http://en.wikipedia.org/wiki/ASCII)



## Códigos ISO-8859

Son 16 códigos distintos de **8 bits**, ampliaciones del ASCII, que permiten usar signos alfabéticos usados en otras lenguas distintas del inglés.

[ISO-8859](http://en.wikipedia.org/wiki/ISO/IEC_8859)



## Latin - 1

En Europa Occidental usamos el conjunto de caracteres **Latin-1** en alguna de sus variantes.

- [ISO-8859-1](http://en.wikipedia.org/wiki/ISO_8859-1)
- [ISO-8859-15](http://en.wikipedia.org/wiki/ISO_8859-15)  (revisión del anterior para añadir el signo del euro €)
- [Windows-1252](http://en.wikipedia.org/wiki/Windows-1252)  (ANSI)



## UNICODE

Es una codificación universal con un repertorio de más de 120000 signos de cualquier alfabeto, cada uno tiene asignado un `code point`.

![](/imagenes/unicode1.png)

[Unicode character table](http://unicode-table.com/en/)



## Direccionalidad y composición

Los interpretes de Unicode manejan automáticamente la dirección de la escritura y la composición de signos.



![img](imagenes/unicode2.png)

![img](imagenes/unicode3.png)



## Formatos de Unicode

Unicode se puede codificar de 3 maneras:

- **UTF-8** (en unix, internet, ...)
- **UTF-16** (en windows)
- **UTF-32**

[UFT-8, UTF-16, UTF-32 & BOM](http://www.unicode.org/faq/utf_bom.html)



## UTF-8

![img](imagenes/utf-8.png)

Codificación variable, compatible ASCII.

Es una **codificación variable de 8 bits** compatible con ASCII.

- **1 byte:** US-ASCII (128 caracteres)
- **2 bytes:** caracteres romances más signos diacríticos, alfabetos griego, cirílico, copto, armenio, hebreo, árabe... (1920 caracteres)
- **3 bytes:** Caracteres del plano básico multilingüe de Unicode: caracteres de uso común, incluyendo chino, japonés y coreano.
- **4 bytes:** Caracteres del plano suplementario multilingüe. Símbolos matemáticos y alfabetos clásicos para uso académico.

[UTF-8](http://en.wikipedia.org/wiki/UTF-8)

**UTF-8** es el más generalizado, sobre todo en Internet y en UNIX. **UTF-16** se usa en Windows.

| &#33865; → 葉 |
| ------------- |
| &#9822; → ♞   |
| &#233; → é    |
| &eacute; → é  |
| &copy; → ©    |

En `html` o `xml` se puede escribir cualquier signo: **&#decimal;** o algunos signos se pueden escribir con [character encodings](http://www.escapecodes.info/).



## Codificación en protocolos de Internet

En internet se presupone `UTF-8` a menos que se indique otra cosa

```html
# En HTML5, después de la declaración
<meta charset="UTF-8">

# En HTML4, y HTML5 en el <head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

# En XML, utf-8 por defecto, si no:
<?xml version="1.0" encoding="ISO-8859-1">
```



## Conversión entre formatos

Se puede cambiar con cualquier editor de texto, o con las utilidades de unix:

[iconv](https://www.systutorials.com/docs/linux/man/3-iconv/)

[recode](https://www.systutorials.com/docs/linux/man/1-recode/)



## Soporte en C++

En Unicode no todos los carácteres tienen 1 byte. Para estos está el tipo `wchar_t` en lugar de `char` y las funciones `wcs`.

[Wide Character](http://en.wikipedia.org/wiki/Wide_character#C.2FC.2B.2B)



## Unicode en python

Desde  Python 3  el tipo  `str` contiene caracteres Unicode. En  Python 2  existe el tipo `unicode` distinto del  `str`.

[Unicode HOWTO](http://docs.python.org/3.8/howto/unicode.html)

### Codificación - Descodificación en Python3

En python3 existen los tipos `str` (unicode) y `binary`.

Para convertir pasar desde / hasta las distintas codificaciones están las funciones  **byte.decode()**  y  **str.encode().**

```python
a = 'cañón'
print (a.encode('utf8'))
b'ca\xc3\xb1\xc3\xb3n'

print (a.encode('latin1'))
b'ca\xf1\xf3n'
```

### Entrada / Salida en Python3

Se trabaja en unicode internamente, y solaménte se debe convertir a o desde una codificación concreta en la entrada / salida. `write y open` codifican y descodifican automáticamente (utf-8 por defecto).

```python
f = open ('texto.txt', 'w', encoding='utf-8')
f.write(a)
f.close()

with open('texto.txt', encoding='utf-8') as f:
   print (f.readline())
```



## Unicode es un concepto 

para guardar en el disco hay que codificar.

| **letter** | **Unicode Code Point** |
| ---------- | ---------------------- |
| ć          | \u0107                 |

| Byte Encodings |           |            |               |
| -------------- | --------- | ---------- | ------------- |
| **letter**     | **UTF-8** | **UTF-16** | **Shift-JIS** |
| ć              | \xc4\x87  | \x07\x01   | \x85\xc9      |



## Más Unicode

[The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets (No Excuses!)](https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/)