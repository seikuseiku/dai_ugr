# Micro Frameworks Web: Flask

Web Applications Frameworks son software para apoyar el desarrollo de aplicaciones web. Vamos a ver Flask (micro framework) y Django.
[Web application framework](http://en.wikipedia.org/wiki/Web_application_framework)
[10 Most Popular Web Frameworks in 2020](https://medium.com/front-end-weekly/10-most-popular-web-frameworks-in-2020-167b9103e08a)

### Flask

Servidor de desarrollo y depurador incorporado.

+ Soporte integrado para pruebas unitarias.
+ Plantillas Jinja2.
+ Soporte para Cookies seguras (sesiones en el lado del cliente).
+ Compatible con Unicode.
+ Profusamente documentado.

[Documentación de Flask](https://flask.palletsprojects.com/en/1.1.x/)

### Hola Mundo con Flask

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')                 # decorador, varia los parametros
def hello_world():              # I/O de la función
    return '¡Hola Mundo!'

# se ejecuta:
$ export FLASK_APP=hola_mundo.py
$ export FLASK_ENV=development     # entorno desarrollo
$ flask run --host=0.0.0.0
 * Running on http://0.0.0.0:5000/
```

[Python decorators](https://www.programiz.com/python-programming/decorator)

### Manejo de URLs con Flask (routing)

Usando el decorador `route()`:

```python
@app.route('/')
def index():
    return 'Página principal'

@app.route('/hola')
def hola():
    return 'Hola'
```

[Routing](https://flask.palletsprojects.com/en/1.1.x/quickstart/#routing)

### Variables en la URL

```python
#/user/zerjillo
#/user/pepe
@app.route('/user/<username>') # Captura una parte del URL
def mostrar_nombre_usuario(username): # y la pasa como parámetro
    return f" Usuario : {username}"
```

[Variable Rules](https://flask.palletsprojects.com/en/1.1.x/quickstart/#variable-rules)

### Entrada de variables GET [parámetros]

```python
# /prog?par1=hola&par2=pepe
from flask import request
...
@app.route('/prog')
def prog():   # los parámetros están en el request
    parametro1 = request.args.get('par1')   # hola
    parametro2 = request.args.get('par2')   # pepe
    return parametro1 + ' ' + parametro2
```

[Request object](https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data)

### Cabeceras HTTP en Flask

```python
from flask import Response

@app.route('/text')
def text():
    response = Response()
    response.headers['Content-Type'] = 'text/plain; charset=iso-8859-1'
    response.set_data("Hola mundo")    # Un string
    return response

@app.route('/png')
def png():
    response = Response()
    response.headers.add('Content-Type', 'image/png')
    response.set_data(imagen_png)   # Datos binarios
    return response

@app.route('/svg')
def svg():
    response = Response()
    response.headers.add('Content-Type', 'image/svg+xm')
    response.set_data(imagen_svg  )   # El XML del gráfico SVG
    return response
```

[Internet media types](http://en.wikipedia.org/wiki/Internet_media_type)

### Métodos HTTP (GET, POST, PUT, ...)

```python
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        hacer_login()
    else:
        enviar_formulario_para_login()
```

[HTTP Methods](https://flask.palletsprojects.com/en/1.1.x/quickstart/#http-methods)

### Redirecciones y errores

```python
from flask import abort, redirect, url_for

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    abort(401)
    estoNoSeEjecuta()

@app.errorhandler(404)
def page_not_found(error):
    return "Página no encontrada", 404
```

[Redirects and errors](https://flask.palletsprojects.com/en/1.1.x/quickstart/#redirects-and-errors)

### Cookies en Flask

[Cookies](https://flask.palletsprojects.com/en/1.1.x/quickstart/#cookies)

Leer cookie: 

```python
from flask import request

@app.route('/')
def index():            # las cookies están en el request
    username = request.cookies.get('username')
      # use cookies.get(key) instead of cookies[key] to not get a
      # KeyError if the cookie is missing.
```

Escribir cookie:

```python
from flask import make_response

@app.route('/')
def index():
    response = make_response("logeado como the_username")
    response.set_cookie('username', 'the_username')
    return response
```

### Sesiones en Flask

Se manda un cookie de sesión con un token aleatorio, y se asocia con un registro persitente en el servidor.

[Sessions](https://flask.palletsprojects.com/en/1.1.x/quickstart/#sessions)

```python
from flask import Flask, session, redirect, url_for, escape, request
app = Flask(__name__)
# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    return '''
        <form method="post">
            <p><input type=text name=username>
           <p><input type=submit value=Login>
        </form>
  '''

@app.route('/')
  def index():
  if 'username' in session:
    return f'Logged in as {escape(session['username']}
  return 'You are not logged in'

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))
```

### Logs en Flask

Registro de eventos usando el módulo [logging](https://docs.python.org/3/library/logging.html) de pyhton. [Logs en Flask](http://lineadecodigo.com/python/logs-en-flask/), [Logging in Python](https://realpython.com/python-logging/).

```python
@app.route('/')
def principal():
  app.logger.debug('Arranque de la aplicacion')
  return 'Ejemplo para logs'
```

### Otros enlaces

# Desarrollo de Aplicaciones para Internet

## Micro Frameworks Web: Flask



![img](./imagenes/web-app-dev.jpg)



[Desarrollo de Aplicaciones para Internet](https://swad.ugr.es/?CrsCod=7257)

## Web Applications Frameworks

Es software para apoyar el desarrollo de aplicaciones web.

Vamos a ver:

- [Flask](http://flask.pocoo.org/)  (un 'micro framework')
- [Django](https://www.djangoproject.com/)

[Web application framework](http://en.wikipedia.org/wiki/Web_application_framework),
[10 Most Popular Web Frameworks in 2020](https://medium.com/front-end-weekly/10-most-popular-web-frameworks-in-2020-167b9103e08a)

## Flask

- Servidor de desarrollo y depurador incorporado
- Soporte integrado para pruebas unitarias
- Plantillas [Jinja2](http://jinja.pocoo.org/docs/dev/)
- Soporte para Cookies seguras (sesiones en el lado del cliente)
- Compatible con Unicode
- **Profusamente documentado**

[Documentación de Flask](http://flask.pocoo.org/docs)

## Hola Mundo con Flask

```
from flask import Flask
app = Flask(__name__)

@app.route('/')                 # decorador, varia los parametros
def hello_world():              # I/O de la función
    return '¡Hola Mundo!'

# se ejecuta:
$ export FLASK_APP=hola_mundo.py
$ export FLASK_ENV=development     # entorno desarrollo
$ flask run --host=0.0.0.0
 * Running on http://0.0.0.0:5000/
```

[Python Decorators](https://www.programiz.com/python-programming/decorator)

## Manejo de URLs con Flask (routing)

Usando el decorador `route()`:



```
@app.route('/')
def index():
    return 'Página principal'

@app.route('/hola')
def hola():
    return 'Hola'
```

[Routing](https://flask.palletsprojects.com/en/1.1.x/quickstart/#routing)

## Variables en la URL



/user/zerjillo





/user/pepe



```
@app.route('/user/<username>') # Captura una parte del URL
def mostrar_nombre_usuario(username): # y la pasa como parámetro
    return f" Usuario : {username}"
```

[Variable Rules](https://flask.palletsprojects.com/en/1.1.x/quickstart/#variable-rules)

## Entrada de variables GET [parámetros]



/prog?par1=hola&par2=pepe



```
from flask import request

...

@app.route('/prog')
def prog():   # los parámetros están en el request
    parametro1 = request.args.get('par1')   # hola
    parametro2 = request.args.get('par2')   # pepe
    return parametro1 + ' ' + parametro2
```

[Request object](https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data)

## Cabeceras HTTP en Flask

```
from flask import Response

@app.route('/text')
def text():
    response = Response()
    response.headers['Content-Type'] = 'text/plain; charset=iso-8859-1'
    response.set_data("Hola mundo")    # Un string
    return response

@app.route('/png')
def png():
    response = Response()
    response.headers.add('Content-Type', 'image/png')
    response.set_data(imagen_png)   # Datos binarios
    return response

@app.route('/svg')
def svg():
    response = Response()
    response.headers.add('Content-Type', 'image/svg+xm')
    response.set_data(imagen_svg  )   # El XML del gráfico SVG
    return response
```

[Internet media types](http://en.wikipedia.org/wiki/Internet_media_type)

## Métodos HTTP (GET, POST, PUT...)

```
@app.route('/login', methods=['GET', 'POST'])

def login():
    if request.method == 'POST':
        hacer_login()
    else:
        enviar_formulario_para_login()
```

[HTTP Methods](https://flask.palletsprojects.com/en/1.1.x/quickstart/#http-methods)

## Redirecciones y Errores

```
from flask import abort, redirect, url_for

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    abort(401)
    estoNoSeEjecuta()

@app.errorhandler(404)
def page_not_found(error):
    return "Página no encontrada", 404
```

[Redirects and Errors](https://flask.palletsprojects.com/en/1.1.x/quickstart/#redirects-and-errors)

## Cookies en Flask

Leer cookie:

```
from flask import request

@app.route('/')
def index():            # las cookies están en el request
    username = request.cookies.get('username')

      # use cookies.get(key) instead of cookies[key] to not get a
      # KeyError if the cookie is missing.
```

[Cookies](https://flask.palletsprojects.com/en/1.1.x/quickstart/#cookies)

## Cookies en Flask

Escribir cookie:

```
from flask import make_response

@app.route('/')
def index():
    response = make_response("logeado como the_username")
    response.set_cookie('username', 'the_username')
    return response
```

[Cookies](https://flask.palletsprojects.com/en/1.1.x/quickstart/#cookies)

## Sesiones en Flask

Se manda un cookie de sesión con un token aleatorio, y se asocia con un registro persitente en el servidor

```
fromfrom flask import Flask, session, redirect, url_for, escape, request

app = Flask(__name__)

# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    return '''
        <form method="post">
            <p><input type=text name=username>
           <p><input type=submit value=Login>
        </form>
  '''

  @app.route('/')
  def index():
  if 'username' in session:
    return f'Logged in as {escape(session['username']}
  return 'You are not logged in'

...

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))
```

[Sessions](https://flask.palletsprojects.com/en/1.1.x/quickstart/#sessions)

### Logs en Flask

Registro de eventos usando el módulo [logging](https://docs.python.org/3/library/logging.html) de pyhton. [Logs en Flask ](http://lineadecodigo.com/python/logs-en-flask/), [Logging in Python](https://realpython.com/python-logging/).

```
@app.route('/')
def principal():
  app.logger.debug('Arranque de la aplicacion')
  return 'Ejemplo para logs'
```

### Otros enlaces

[15 Useful Flask Extensions and Libraries That I Use in Every Project](https://nickjanetakis.com/blog/15-useful-flask-extensions-and-libraries-that-i-use-in-every-project)

[Flask tutorials](https://www.youtube.com/watch?v=mqhxxeeTbu0&list=PLzMcBGfZo4-n4vJJybUVV3Un_NFS5EOgX)