# Prácticas de Desarrollo de Aplicaciones para Internet (DAI)

**Contenidos e índice de prácticas**

1. Python y entorno de trabajo con Docker.
2. [**FLASK**] Microframework Flask.
3. [**FLASK**] Plantillas, manejo de sesiones y frameworks CSS.
4. [**FLASK**] Bases de datos NoSQL y CRUD.
5. [**FLASK**] API REST.
8. [**FLASK**] Front-End con jQuery.
9. [**FLASK**] Despliegue en producción.
10. [**FLASK**] Javascript.

---



## Cómo acceder a la práctica.

Primero, iniciar el contenedor Docker:

```
sudo docker-compose up
```

Segundo, recuperar la base de datos de MongoDB desde otra terminal:

```
docker-compose exec mongo /bin/bash
mongorestore --drop dump
exit
```

Tercero, acceder a la web mediante *localhost* en el navegador.

---



## Tour básico por la práctica

### 1. Página principal.

![](/imagenes/1_paginaprincipal.png)

La página principal cuenta con un menú a la izquierda desde el que se puede acceder a los contenidos de la web, como la lista de películas (2), la lista de películas desde la API REST (3), gráficas de análisis (4). También cuenta con inicio de sesión y registro, modo noche y las últimas tres páginas visitadas.



## 2. Lista de películas

![](/imagenes/2_listadopeliculas.png)

Listado de todos los elementos de la colección de MongoDB usando paginación, con capacidad de añadir nuevos elementos (en el formulario del botón *Crear nueva película*), editar los datos del elemento y  eliminarlo (en el botón con el icono de configuración), y realizar una búsqueda (barra de búsqueda).



## 3. Lista de películas usando la API REST

![](/imagenes/3_listadopeliculasAPI.png)

Listado de todos los elementos de la colección de MongoDB usando llamadas a la API REST, con la posibilidad de eliminar los elementos.

Las posibles llamadas a la API REST desde la terminal son las siguientes, con algunos ejemplos:

+ GET

```
http localhost/api/movies
http localhost/api/movies?title="Star Wars"
http localhost/api/movies?year="2020"
http localhost/api/movies?valoracion="5"
http localhost/api/movies?premio="Goya"
```

+ POST

```
http -f POST http://localhost/api/movies title="Star Wars: Episode VII - The Force Awakens" year=2015 imdb=tt2488496 type=movie
```

+ GET (película)

```
http localhost/api/movies/[ID]
```

+ PUT

```
http -f PUT http://localhost/api/movies/[ID] title="Star Wars" year=2015 imdb=tt2488496 type=movie
```

+ DELETE

```
http DELETE localhost/api/movies/[ID]
```



## 4. Gráficas

![](/imagenes/4_graficas.png)

Usando la librería de javascript [Chart JS](https://www.chartjs.org/) se dibujan dos gráficas a partir de los datos sobre películas, conseguidos a través de la API REST.

La primera gráfica muestra la valoración media según los premios que reciben las películas, y la segunda gráfica el número de películas y la valoración media por año. 

Para más información sobre la realización de este apartado se puede consultar el pdf *memoriapractica10.pdf*.

