# Prácticas de Desarrollo de Aplicaciones para Internet (DAI)

**Índice de prácticas**

1. Python y entorno de trabajo con Docker.
2. [**FLASK**] Microframework Flask.
3. [**FLASK**] Plantillas, manejo de sesiones y frameworks CSS.
4. [**FLASK**] Bases de datos NoSQL y CRUD.
5. [**FLASK**] API REST.
6. [**DJANGO**] Django.
7. [**DJANGO**] Autentificación.
8. [**FLASK**] Front-End con jQuery.
9. [**FLASK**] Despliegue en producción.
10. [**FLASK**] Javascript.

---

## Cómo iniciar el entorno de prácticas.

#### Para la práctica de Flask:



---

## Práctica 1: Python y entorno de trabajo con Docker.

Creación del contenedor Docker y pequeños ejercicios usando Python.

### 1.1. Instalación.

El primer paso es instalar Docker y Docker Compose.

Crear el archivo `docker-compose.yml` para ejecutar los ejercicios de este guión. En este archivo especificamos que se monte un directorio `ejercicios`, situado en el mismo directorio que el archivo `yml`.

```yml
# docker-compose.yml
version: '3.7' 
  services:
    contenedor:
      image: python:3.7-alpine
      volumes:
        - ./ejercicios:/ejercicios
      working_dir: /ejercicios
```

y el primer ejercicio:

```python
# ejercicios/hola.py
print("Hola mundo!")
```

Ahora ejecutamos `ejercicios/hola.py` dentro del contenedor `ejercicios` con el comando:

```bash
> docker-compose run contenedor python hola.py
```

### 1.2. Ejercicios

1. Programe un mini-juego de "adivinar" un número (entre 1 y 100) que el ordenador establezca al azar. El usuario puede ir introduciendo números y el ordenador le responderá con mensajes del estilo *"El número buscado el mayor / menor"*. El programa debe finalizar cuando el usuario adivine el número (con su correspondiente mensaje de felicitación) o bien cuando el usuario haya realizado 10 intentos incorrectos de adivinación.
2. Programe un par de funciones de ordenación de matrices (UNIDIMENSIONALES) de números distintas (burbuja, selección, inserción, mezcla, montículos...) ([Wikipedia: Algoritmo de ordenamiento](http://es.wikipedia.org/wiki/Algoritmo_de_ordenamiento)). Realice un programa que genere aleatoriamente matrices de números aleatorios y use dicho métodos para comparar el tiempo que tardan en ejecutarse.
3. La [Criba de Eratóstenes](https://es.wikipedia.org/wiki/Criba_de_Erat%C3%B3stenes) es un sencillo algoritmo que permite encontrar todos los números primos menores de un número natural dado. Prográmelo.
4. Cree un programa que lea de un fichero de texto un número entero `n` y escriba en otro fichero de texto el `n-ésimo` número de la [sucesión de Fibonacci](https://es.wikipedia.org/wiki/Sucesi%C3%B3n_de_Fibonacci).
5. Cree un programa que:
   - Genere aleatoriamente una cadena de `[` y `]`.
   - Compruebe mediante una función si dicha secuencia está *balanceada*, es decir, que se componga de parejas de corchetes de apertura y cierre correctamente anidados. Por ejemplo:
     - `[]` -> Correcto
     - `[[][[]]]` -> Correcto
     - `[][]` -> Correcto
     - `][` -> Incorrecto
     - `[[][[` -> Incorrecto
     - `[]][[]` -> Incorrecto

6. Utilizando [expresiones regulares](http://docs.python.org/3.7/library/re.html) realice funciones para:
   - Identificar cualquier palabra seguida de un espacio y una única letra mayúscula (por ejemplo: `Apellido N`).
   - Identificar correos electrónicos válidos (empieza por una expresión genérica y ve refinándola todo lo posible).
   - Identificar números de tarjeta de crédito cuyos dígitos estén separados por `-` o espacios en blanco cada paquete de cuatro dígitos: `1234-5678-9012-3456` ó `1234 5678 9012 3456`.

---

## Práctica 2: Microframework Flask

Construcción de pequeñas aplicaciones web usando las opciones más básicas del micro framework web para Python [Flask](https://palletsprojects.com/p/flask/). 

### 2.1. Cómo funciona la aplicación

Esta web muestra los ejercicios 2, 3, 4, 5 y 6 de la [Práctica 1](../p1/) y una imagen SVG aleatoria en diferentes URLs:

+ **Index**: `localhost:5000/`  
  Mensaje de bienvenida.
+ **Index de ejercicios**: `localhost:5000/static/index.html`  
  HTML servido como estático en el que hay una lista de enlaces para acceder a los siguientes enlaces a los ejercicios.
+ **Ejercicio 2**: `localhost:5000/ejercicio2/5,2,7,3`  
  Ordenamiento de vector separado por comas usando el algoritmo de ordenación burbuja.
+ **Ejercicio 3**:  `localhost:5000/ejercicio3/30`  
  Obtención de los números primos hasta el número aportado, usando el algoritmo de la Criba de Eratóstenes.
+ **Ejercicio 4**: `localhost:5000/ejercicio4`  
  (Crear y) leer un número n del archivo `file1.txt` y escribir en `file2.txt` el n-ésimo número de la sucesión de Fibonacci.
+ **Ejercicio 5**: `localhost:5000/ejercicio5/[][]`  
  Comprobación de balanceo de secuencias de corchetes.  
+ **Ejercicio 6**: `localhost:5000/ejercicio6/ana@taco.es`  
  Detección de nombres, emails y números de tarjetas de crédito.
+ **Imagen SVG**: `localhost:5000/svg`  
  Imagen SVG aleatoria: círculo, elipse, rectángulo y polígono.

### 2.2. Descripción de taras de la práctica 2

#### 2.2.1. Ejercicios:

1. Hacer una interfaz web para los ejercicios 2-6 de la práctica anterior. La entrada se hará por URL p.e. `http://localhost:5000/ordena/5,2,7,3`.  
   En [Routing](https://flask.palletsprojects.com/en/1.1.x/quickstart/#routing) y [Variable Rules](https://flask.palletsprojects.com/en/1.1.x/quickstart/#variable-rules) está la documentación para ello.

2. Crear una página servida desde un directorio **static**, con las direcciones del ejercicio anterior.

3. Crear una página para el caso en que una URL no esté definida (error `[HTTP 404, not found](http://en.wikipedia.org/wiki/HTTP_404)`).  
   En la [documentación de flask](https://flask.palletsprojects.com/en/1.1.x/quickstart/#quickstart) encontrará como hacer esto.

#### 2.2.2. (Para Nota) Crear Imágenes Dinámicas [Vectoriales]

Desarrolle una aplicación web sencilla que nos permita crear una imagen [SVG](http://es.wikipedia.org/wiki/Scalable_Vector_Graphics) dinámica (que cambie cada vez que visitemos la página) y aleatoria. Por ejemplo, que cada vez que se visite la página dibuje elipses, rectángulos, etc. de colores y posiciones distintas.

Más información sobre como crear SVGs en [https://www.mclibre.org/consultar/htmlcss/html/svg-formas-1.html](https://www.mclibre.org/consultar/htmlcss/html/svg-formas-1.html).

### 2.3. Cómo instalar el contenedor con Flask y Hola Mundo

El archivo **dockerfile**:

```dockerfile
# Dockerfile
FROM python:3.7-alpine

WORKDIR /app  # directorio dentro del contenedor para el código
COPY . /app
RUN pip install -r requirements.txt
        
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_ENV development        
        
CMD ["flask", "run"]
```

El archivo **requirements.txt** con las librerías y versiones para `pip`

```bash
# requirements.txt
flask==1.1
```

El archivo **docker-compose-yml** para usar esta imagen:

> Donde especificamos que una imagen creada desde el archivo **dockerfile** en el *buildcontext de docker* (nuestro directorio) se comunicará por el **puerto 5000** y tendrá montada la carpeta **app** con el código de la aplicación.

```dockerfile
# docker-compose.yml
version: '3.7'
  services: 
    app:
      build: .
      ports:
        - 5000:5000
      volumes:
        - ./app:/app
```

El **código de la aplicación**, por ejemplo el tutorial de "Hello World!", que puede ser modificado desde fuera del contenedor porque está en la carpeta app compartida:

```python
#./app/app.py
from flask import Flask
app = Flask(__name__)
          
@app.route('/')
  def hello_world():
    return 'Hello, World!'
```

Si en algún momento se cambia el **Dockerfile** hay que volver a construir la imágen con:

```bash
> docker-compose build
```

La aplicación se pone en marcha con:

```bash
> docker-compose up
```

---

## Práctica 3: Plantillas, manejo de sesiones y frameworks CSS

Avance en el uso de Flask, con el uso del motor de plantillas Jinja, un framework CSS para hacer HTML responsive y uso de sesiones para gestionar la identificación de usuarios y otros posibles datos de sesión de la web. Incorporación de persistencia con el uso de Pickleshare.

### 3.1. Cómo funciona la aplicación

Esta web tiene una barra de navegación en la que hay enlaces a los ejercicios de las prácticas anteriores, incorpora la funcionalidad de registro, login, visualización y modificación de datos de usuarios y muestra las últimas tres páginas visitadas.

### 3.2. Descripción de tareas de la práctica 3

#### 3.2.1. Plantillas

Gracias a las plantillas conseguimos separar correctamente el aspecto de la aplicación (vista) de su lógica interna (controlador). [MVC](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador).

Flask incorpora el motor de plantillas Jinja2 para generar el HTML (es muy parecido a twig). Para más información sobre ellas, leer las transparencias de clase o entrar en [Flask Tutorial: Templates](https://pythonbasics.org/flask-tutorial-templates/). Además, se puede usar la [herencia de plantillas](https://svn.python.org/projects/external/Jinja-1.1/docs/build/inheritance.html).

#### 3.2.2. Frameworks CSS

El uso de una librería de [CSS Framework](https://en.wikipedia.org/wiki/CSS_framework) nos permitirá un diseño consistente y estandarizado, adaptable al tamaño de pantalla del cliente. Boostrap es el más usado y sencillo, pero se puede usar cualquier otro (como Materialize CSS, Semantic UI, Material UI, UIKit y Foundation).

#### 3.2.3. Maquetación

En el sitio web habrá (al menos) una barra de navegación, un menú vertical, un espacio para mostrar contenidos y un pie de página. La barra de navegación contendrá el logo y el nombre del sitio, enlaces a otras páginas y un pequeño formulario (o un enlace al mismo) para el login.

Podemos empezar por la [plantilla inicial de boostrap](https://getbootstrap.com/docs/3.3/getting-started/#template), a la que añadiremos una barra de navegación como en [Sticky footer with fixed navbar](https://getbootstrap.com/docs/4.0/examples/sticky-footer-navbar/) (inspeccionar el código fuente para ver cómo). En [Bootstrap Navigation Bar](https://www.w3schools.com/bootstrap/bootstrap_navbar.asp) y en la [documentación de boostrap](https://getbootstrap.com/docs/4.1/components/navbar/) hay más información sobre las barras de navegación.

A continuación añadiremos el menú vertical a la izquierda y el espacio para los contenidos. Se debe hacer uso del [sistema de grid](https://getbootstrap.com/docs/4.0/layout/grid/) y de las utilidades para el [espaciado y margenes](https://getbootstrap.com/docs/4.0/utilities/spacing/) de bootstrap (o del framework CSS que hayamos elegido).

#### 3.2.4. Contenido

La aplicación tendrá la funcionalidad de login en un espacio de la barra de navegación, de manera que un usuario no logeado pueda logearse o registrarse, y para un usuario registrado se muestre un mensaje de bienvenida y un enlace para deslogearse. En [Message Flashing](https://flask.palletsprojects.com/en/1.1.x/patterns/flashing/#message-flashing-pattern) hay un bosquejo de como hacerlo.

En la barra de navegación habrá varios enlaces, uno de los cuales dará acceso a una interfaz del ejercicio 1 de la primera práctica. Para esto habrá que usar los [formularios](https://www.w3schools.com/bootstrap/bootstrap_forms.asp) de [bootstrap](https://getbootstrap.com/docs/4.0/components/forms/).

En el menú de la derecha aparecerán enlaces a las tres últimas páginas visitadas en su orden.

**Para Nota**: Poner en los enlaces de la barra de navegación el interface a todos los ejercicios de la primera práctica con un menu dropdown.

#### 3.2.5. Manejo de sesiones

En toda aplicación web es necesario gestionar la información que se mantenga entre las distintas páginas que visita el usuario. Para ello se hace uso de las [sesiones](http://lineadecodigo.com/python/sesion-en-flask/).

#### 3.2.6. Persistencia

Para almacenar la información de nuestra aplicación podemos utilizar distintos mecanismos de persistencia. Cuando la aplicación sea suficientemente sencilla podemos utilizar algún esquema de almacenamiento local, como por ejemplo la biblioteca [`pickleshare`](https://pypi.org/project/pickleshare/).

En esta práctica usaremos dicha base de datos para almacenar la información del usuario que se registra a través de un formulario de registro en nuestra aplicación web. 
Crearemos además una página nueva que nos permita visualizar los datos del usuario (tendremos acceso a través del menú a esta página cuando estemos identificados en la web). 
Asimismo crearemos una nueva página que le permita al usuario cambiar los datos personales (volviendo a mandar el formulario, que aparecerá relleno con los datos anteriores).

Para instalar `pickleshare` tendremos que añadirla como dependencia en nuestro fichero de **requirements.txt** (y reconstruir el contenedor para que incluya esta librería).

#### 3.2.7. Modelo-Vista-Controlador

Debemos intentar seguir el paradigma *modelo, vista, controlador* (MVC) lo más fielmente posible, para que sea fácil poder cambiar radicalmente el aspecto de la aplicación web modificando únicamente los templates, o cambiar la base de datos sin afectar al resto del código. 
Los archivos de `HTML` estarán, por tanto, en un directorio **templates** separado, y lo relacionado con la persistencia estará en un archivo (módulo de python) aparte (en **model.py**).

#### 3.2.8. Posible problema en el uso de PickleShareDB

En esta práctica se ha propuesto el uso de `PickleShareDB`, que es una biblioteca que permite de manera muy básica que almacena en disco un diccionario.

Su manejo es extremadamente simple: al asignar el valor de uno de los elementos del diccionario se guardarán dichos cambios en el disco para que en una posterior ejecución se pueda leer y usar dicha información. 

Sin embargo es posible que nos estemos enfrentando a un pequeño problema. Si ejecutamos:

```python
db = PickleShareDB('miBD')
db['pepe'] = dict()
db['pepe']['pass'] = '1234'
```

Podremos comprobar al relanzar nuestra aplicación que `db['pepe']` es un diccionario vacío (y hemos perdido el valor de `'pass'`).

Este problema ocurre porque la biblioteca `PickleShareDB` no puede detectar cambios en objetos mutables que se usen su diccionario. Es decir, no se da cuenta que hemos cambiado el objeto diccionario que habíamos creado en la segunda línea del ejemplo anterior.

**Posibles soluciones**

Existen varias opciones que podemos usar para evita este problema. Por ejemplo:

```python
db = PickleShareDB('miBD')
db['pepe'] = {'pass': '1234'}
```

En este caso estamos creando el diccionario y rellenándolo ANTES de hacer la asignación en la base de datos de `PickleShareDB`. Otra posible solución es forzar la grabación del objeto mutable después de haberlo cambiado:

```python
db = PickleShareDB('miBD')
db['pepe'] = dict()
db['pepe']['pass'] = '1234'+
db['pepe'] = db['pepe']
```

La última línea de este código parece redundante, pero en realidad lo que está consiguiendo con esa asignación al diccionario de `PickleShareDB` es que se active la grabación de la información en el disco.

---

# Práctica 4: Bases de datos NoSQL y CRUD

Utilización de base de datos NoSQL: MongoDB, CRUD y paginación.

### 4.1. Cómo funciona la aplicación

En el menú de la izquierda se puede acceder a la colección *Películas*, donde se muestra una base de datos de películas en la que se puede añadir, buscar, editar y borrar los registros. Además cuenta con paginación.

### 4.2. Descripción de tareas de la práctica 4

#### 4.2.1. Bases de datos NoSQL: mongoDB

[`MongoDB`](https://docs.mongodb.com/drivers/) es una base de datos NoSQL potente en la que almacenaremos cualquier información de nuestra web, a la que accederemos desde Python usando el cliente [`Pymongo`](https://pymongo.readthedocs.io/en/stable/).

Se usarán algunos datos de prueba para `mongoDB` (siguiente apartado), con la idea de crear varias páginas web para consultar información sobre estos datos, hacer una búsqueda sencilla, insertar nuevos elementos, modificar los ya existentes, etc.

#### 4.2.2. Instalación del servicio mongoDB y de bases de datos de prueba

Usaremos una imagen de [mongo](https://hub.docker.com/_/mongo) en docker para montar el servicio de la base de datos.

El archivo *docker-compose.yml* es:

```dockerfile
# docker-compose.yml
version: '3.7'

services:
	app:
		build: .
		depends_on:
			- mongo
		ports:
			- 5000:5000
		volumes:
			- ./app:/app
		working_dir: /app

	mongo:
		image: mongo:4
		restart: always					# reinicio si se cuelga
		ports:
			- 27017:27017
		volumes:
			- ./dump:/dump              # los datos de prueba
			- ./datos_db:/data/db       # almacenamiento en el host
			
	mongo-express:
		image: mongo-express
		restart: always
		depends_on:
			- mongo
		ports:
			- 8081:8081
```

Crearemos (en el mismo nivel que el archivo *docker-compose*) los directorios **dump** para los datos de prueba y **datos_db** para el almacenamiento de los datos de la DB en el host para ***no perder los datos en cada build***.

Además, añadimos un nuevo requisito a *requirements.txt*:

```python
# requirements.txt
pymongo==3.11
```

Nos aseguramos de que la imagen de Flask se construye con la nueva librería:

```
> docker-compose build
```

Llegados a este punto ya podríamos manejar la DB desde la aplicación de Flask usando pymongo pero aún no tenemos datos sobre los que trabajar, por lo que vamos a descargar [colecciones de Internet](https://github.com/SouthbankSoftware/dbkoda-data).

En la carpeta **dump** creamos una subcarpeta **SampleCollections** para una BD con este nombre, y dentro de ella ponemos las colecciones que elijamos de [SampleCollections](https://github.com/SouthbankSoftware/dbkoda-data/tree/master/SampleCollections/dump/SampleCollections). 

Para restaurar la BD ponemos a funcionar los servicios:

```
> docker-compose up
```

y en otra terminal abrimos una sesión de bash en el contenedor de mongo:

```
#> docker-compose exec mongo /bin/bash
```

y en la terminal del contenedor restauramos la base de datos *SampleCollections* con las colecciones que tenga:

```
#> mongorestore --drop dump
```

Aquí podemos comprobar que los datos están usando el [shell de mongo](https://docs.mongodb.com/manual/mongo/). En http://www.diegocalvo.es/tutorial-de-mongodb-con-ejemplos/ tenemos una chuletas para usarlo.



Si todo ha ido bien ya podríamos hacer que nuestra aplicación web muestre información de nuestra base de datos. Por ejemplo: el título de un capítulo de la serie Friends:

```python
#./app/app.py
from pymongo import MongoClient

client = MongoClient("mongo", 27017) # Conectar al servicio (docker) "mongo" en su puerto estandar
db = client.SampleCollections        # Elegimos la base de datos de ejemplo

...

@app.route('/mongo')
def mongo():
	# Encontramos los documentos de la coleccion "samples_friends"
	episodios = db.samples_friends.find() # devuelve un cursor(*), no una lista ni un iterador

	lista_episodios = []
	for episodio in episodios:
		app.logger.debug(episodio) # salida consola
		lista_episodios.append(episodio)

	# a los templates de Jinja hay que pasarle una lista, no el cursor
	return render_template('lista.html', episodios=lista_episodios)
```

> (*) [find()](https://api.mongodb.com/python/current/api/pymongo/collection.html#pymongo.collection.Collection.find)



Hacer una búsqueda simple con un formulario para la colección elegida. [Pymongo Tutorial](https://api.mongodb.com/python/current/tutorial.html) sería la documentación a consultar para las búsquedas. También puede ser útil la [búsqueda con expresiones regulares](https://docs.mongodb.com/manual/reference/operator/query/regex/).

**Para nota**: Incluir todo el CRUD con la modificación parcial, borrado y adicción de nuevos documentos.

#### 4.2.3. Mongo Express

Tras instalar el *docker-compose.yml* de más arriba, podemos acceder a **Mongo Express** con [localhost:8081](localhost:8081), donde podríamos crear la base de datos, ver los registros, etc.

---

## Práctica 5: API REST

Funcionalidad de CRUD sobre la base de datos NoSQL MongoDB usando una API RESTfull.

### 5.1. Cómo funciona la aplicación

Para probar los *end points* o *urls* podemos usar las utilidades [curl](https://www.hostinger.es/tutoriales/comando-curl/) o [httpie](https://httpie.io/) en una terminal desde la carpeta `practica_flask`. Se devuelve un JSON con la información que se pida. Este es el orden en el que defendí la API REST en la defensa de prácticas:

+ **GET**

  Para listar todas las películas: `http localhost:5000/api/movies`
  Para buscar por título: `http localhost:5000/api/movies?title="Star Wars"`
  Para buscar por año: `http localhost:5000/api/movies?year="2020"`

+ **POST**
  Parar introducir una nueva película en la base de datos:`http -f POST http://localhost:5000/api/movies title="Star Wars: Episode VII - The Force Awakens" year=2015 imdb=tt2488496 type=movie`
+ **GET movie**
  Listar información de una película: `http localhost:5000/api/movies/[ID]`
+ **PUT**
  Actualizar información de una película: `http -f PUT http://localhost:5000/api/movies/[ID] title="Star Wars" year=2015 imdb=tt2488496 type=movie`
+ **DELETE**
  Borrar una película: `http DELETE localhost:5000/api/movies/[ID]`

### 5.2. Descripción de tareas de la práctica 5

#### 5.2.1. Descripción de la práctica

En esta práctica haremos la misma funcionalidad de la práctica anterior (CRUD sobre la BD), pero usando una API RESTfull. Lo haremos siguiendo las indicaciones de [What is a REST API?](https://www.youtube.com/watch?v=SLwpqD8n3d0), y las directrices de [RESTful](https://restfulapi.net/rest-architectural-constraints/):

- Usar los métodos de HTTP explicitamente
- Sin estado, cacheable
- Exponer urls estilo path, interface uniforme
- Responder JSON o XML

#### 5.2.2. End points

Lo primero será planificar que urls, con que verbos, y que respuesta tendrán. Si tengo la colleción de movies:

- `GET /api/movies`, devolverá un lista con todos los registros
- `GET /api/movies?year=2014`, devolverá un lista esta búsqueda
- `POST /api/movies`, creará un registro nuevo a partir de los parámetros que enviemos con el POST, y devolverá el `id` del registro creado, y sus datos
- `PUT /api/movies/tt1155056`, modificará el registro con `id` tt1155056 a partir de los parámetros enviados en el POST y devolverá el `id` del registro modificado y sus datos
- `DELETE /api/movies/tt1155056`, borrara el registro con `id` tt1155056 y devolverá el `id` del registro borrado

En su caso, devolverá los mesajes de error correspondientes. El código sería:

```python
#./app/app.py
from flask import request, jsonify
from bson import ObjectId
...
# para devolver una lista (GET), o añadir (POST)
@app.route('/api/movies', methods=['GET', 'POST'])
def api_1():
    if request.method == 'GET':
        lista = []
        movies = db.video_movies.find().sort('year')
        for movie in movies:
            lista.append({
                  'id':    str(movie.get('_id')), # pasa a string el ObjectId
                  'title': movie.get('title'), 
                  'year':  movie.get('year'),
                  'imdb':  movie.get('imdb')
                })
        return jsonify(lista)

        if request.method == 'POST':
            ...

# para devolver una, modificar o borrar
@app.route('/api/movies/<id>', methods=['GET', 'PUT', 'DELETE'])
def api_2(id):
    if request.method == 'GET':
        try:
            movie = db.video_movies.find_one({'_id':ObjectId(id)})
            return jsonify({
                'id':    id,
                'title': movie.get('title'), 
                'year':  movie.get('year'),
                'imdb':  movie.get('imdb')
            })
        except:
          return jsonify({'error':'Not found'}), 404
    ...  
    
```

Para probarlo podemos usar las utilidades [curl](https://www.hostinger.es/tutoriales/comando-curl/) o [httpie](https://httpie.io/).

##### Para nota (apartado no hecho)

Hacer otra versión del api usando la librería [flask-restfull](https://flask-restful.readthedocs.io/en/latest/quickstart.html), como en [Python REST API Tutorial - Building a Flask REST API](https://www.youtube.com/watch?v=GMppyAPbLYk)

---

## Práctica 6: Django

[Práctica no hecha]

### 6.1. Cómo funciona la aplicación

[Práctica no hecha]

### 6.2. Descripción de tareas de la práctica 6

#### 6.2.1. Resumen

`Django` es un framework web completo ampliamente usado. Cuenta con un motor de plantillas propio (muy similar a `Jinja 2`) así una arquitectura `Modelo/Vista/Controlador`. En esta práctica vamos a instalar dicho framework y a hacer una aplicación básica.

#### 6.2.2. Instalación y puesta en marcha de `Django`

Al ser un framework tan usado, en internet podemos encontrar muchos tutoriales como [Django Girls](https://tutorial.djangogirls.org/es/) o [Django Tutorial for Beginners](https://data-flair.training/blogs/django-tutorial/)

Seguiremos los pasos de [Quickstart: Compose and Django](https://docs.docker.com/compose/django/) para hacer la instalación con docker.

Siguendo las indicaciones de la página anterior, para crear nuestro proyecto ejecutamos **dentro** del contendor de django:

```bash
$ docker-compose run web django-admin mi_sitio_web .
```

Una vez instalado, podemos compropbar que funciona iniciando el servidor de desarrollo:

```bash
$ docker-compose up
```

Para la Base de Datos, podemos usar `postgres` como en la página anterior, o dejar `sqlite` que trae por defecto django. En caso de usar `postgres`, es conveniente montar un directorio para los datos como indica final de la página de la documentación de la [imagen docker de postgress](https://hub.docker.com/_/postgres), y añadir la imagen de adminer si se desea un cliente gráfico para la BD como pone en esta documentación.

Creamos ahora una aplicación dentro del projecto:

```bash
$ docker-compose run web python manage.py startapp mi_aplicacion
```

Creamos un directorios para los templates y para los archivos estáticos:

```bash
$ mkdir app/mi_aplicacion/templates
$ mkdir static
```

y los apuntamos en el archivo `sitio_web/settings.py`

```python
# al final del archivo settings.py
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
```

y apuntamos también nuesta aplicación:

```python
INSTALLED_APPS = (
  'django.contrib.admin',
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.messages',
  'django.contrib.staticfiles',
  'mi_aplicacion',
)
```

Ahora podemos iniciar la bases de datos SQL que usa django para los datos de los usuarios (registro, autentificación y autorización), que usaremos más adelante.

```bash
$ docker-compose run web python manage.py migrate
```

Creamos ahora un administrador de la BD (SQL)

```bash
$ docker-compose run web python manage.py createsuperuser
```

y tendremos acceso a la aplicación de administración de la BD en:

```bash
 http://localhost:8000/admin
```



Y podemos ahora hacer una aplicación siguiendo los pasos de [URLs en Django](https://tutorial.djangogirls.org/es/django_urls/)

Solo tendremos que cambiar, el enrutador (ahora en dos archivos aparte) `mi_sitio_web/urls.py`:

```python
# mi_sitio_web/urls.py

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
  url('admin/', admin.site.urls),
  url('', include('mi_aplicacion.urls')),
]
```

y en un nuevo archivo donde especificamos las rutas que comiencen por **/**, `mi_aplicacion/urls.py`

```python
# mi_aplicacion/urls.py

from django.urls import path
from . import views

urlpatterns = [
  path('', views.index, name='index'),
  path('test_template', views.test_template, name='test_template'),
]
```



El código del controlador lo pondremos en el archivo `miaplicacion/views.py`

```python
# mi_aplicacion/views.py

from django.shortcuts import render, HttpResponse

# Create your views here.

def index(request):
    return HttpResponse('Hello World!')

def test_template(request):
    context = {}   # Aquí van la las variables para la plantilla
    return render(request,'test.html', context)
```



`Django` utiliza una [libreria de templates](https://docs.djangoproject.com/en/1.11/ref/templates/), muy parecida al `Jinja2` de `Flask`: solo cambian las instrucciones para cargar los archivos estaticos y los nombres de los enlaces

```markup
{% load static %}
   ...
  <link  href="{% static 'css/style.css' %}" rel="stylesheet">
  ...
  <a href="{% url 'name para la url' %}"> ... </a>
```



#### 6.2.3.Model

Haremos una aplicación para gestionar los prestamos de una biblioteca con dos tablas para libros, y prestamos. El código para el [model](https://docs.djangoproject.com/en/3.1/topics/db/models/), va en el archivo **model.py**, el modelo mas básico sería:

```python
# mi_aplicacion/models.py
from django.db import models
from django.utils import timezone

class Libro(models.Model):
  titulo = Models.CharField(max_lenght=200)
  autor  = Models.CharField(max_length=100)
  ... 

  def __str__(self):
    return self.titulo

class Prestamo(models.Model):
  libro   = Models.ForeingKey(Libro, on_delete=models.CASCADE)
  fecha   = Models.DateField(default=timezone.now)
  usuario = Models.CharField(max_lenght=100)
  
  ...
```



**Opcionalmente** este modelo se puede ampliar con un campo para subir un archivo de imagen para la portada del libro, usar otra tabla para autores, de forma que un libro pueda tener varios autores, y un autor varios libros, etc

Cada vez que toquemos la estructura de las tablas, tendremos que pasar los scripts de `makemigrations` y `migrate` ([Migrations](https://docs.djangoproject.com/en/3.1/topics/migrations/)).

#### 6.2.4. Formularios

Django incorpora un mecanismo para manejar los formularios de manera eficiente. En este apartado se pide crear los formularios que permitan añadir, editar y borrar los modelos. Por supuesto el formulario debe realizar todas las validaciones pertinantes (evitar campos en blanco, valores incorrectos, etc).

##### **Para nota**

Integrar los formularios con boostrap, como en [Advanced Form Rendering with Django Crispy Forms](https://simpleisbetterthancomplex.com/tutorial/2018/11/28/advanced-form-rendering-with-django-crispy-forms.html)

---

## Práctica 7: Autentificación

[Práctica no hecha]

### 7.1. Cómo funciona la aplicación

[Práctica no hecha]

### 7.2. Descripción de tareas de la práctica 7

#### 7.2.1. Resumen

En esta práctica añadiremos la autentificación/registro de usuarios a la práctica anterior, de manera que para pedir el prestamo de un libro haya que estar registrado y para cambiar la tabla de libros, el usuario tenga que estar registrado como staff.

Usaremos la aplicación de [autentificación de Django](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Authentication), o el plugin [all-auth](https://django-allauth.readthedocs.io/en/latest/), que incluye también autentificación delegada en redes sociales. En [Django-allauth Tutorial](https://learndjango.com/tutorials/django-allauth-tutorial) cuentan como hacerlo.

También es conveniente relacionar los usuarios con los de Django en la tabla de `Prestamos`

```python
from django.contrib.auth.models import User
...
class Prestamo(models.Model):
  ...
  usuario = models.ForeignKey(User, on_delete=models.CASCADE)
```

#### 7.2.2. Templates

Estas aplicaciones traen sus propios templates, para poder utilizar otros, tendremos que poner en el archivo **settings.py**

```python
# mi_sitio_web/settings.py
 ...
  TEMPLATES = [
  {
      'BACKEND': 'django.template.backends.django.DjangoTemplates',
      'DIRS': [os.path.join(BASE_DIR), 'templates'],
      'APP_DIRS': True,
      ...
  },
]
...
```

y crear un sub directorio **templates**, en el directorio donde está **manage.py** para poner los templates,
 **.**
├── **templates**
│     └── **account**
│             └── login.html
│             └──  ...
├── manage.py
├──  ...

En [allauth templates](https://github.com/pennersr/django-allauth/tree/master/allauth/templates), y [allauth templates bootstrap4](https://github.com/rrmerugu/django-user-registration-bs4/tree/master/webapp/user_registration_bs4/templates)  hay plantillas ya hechas.

----

## Práctica 8: Front-End con jQuery

[Práctica en proceso]

### 8.1. Cómo funciona la aplicación

[Práctica en proceso]

### 8.2. Descripción de tareas de la práctica 8

#### 8.2.1. Resumen

En esta última práctica usaremos la librería [jQuery](https://jquery.com/) parqa hacer un front end que utilice el API de la práctica 5 y para dar alguna funcionalidad extra a la página.

Utilizaremos las funciones [AJAX](https://uniwebsidad.com/libros/fundamentos-jquery/capitulo-7/metodos-ajax-de-jquery) de jQuery para hacer las llamadas asíncronas. Para esto debemos usar la version `minificada` de jQuery, no la `slim` que es la que viene con boostrap que no las trae.

El front end, tendrá la funcionalidad de buscar registros en la BD, mostrando los resultados en una tabla. Cada una de las filas de la tabla tendrá un botón que permitirá borrar el registro. Además pondremos un botón 'modo nocturno', que nos cambie a un fondo oscuro.

#### 8.2.2. Código

Tenemos que tener en cuenta que el DOM inicial al cargar la página no va a ser el mismo que después de la llamada AJAX, en la que incorporaremos nuevos elementos que no exisiten en la carga inicial

```javascript
// código jQuery que se ejecuta al cargar la página
$(function () {

// evento para cuando cambia el valor introducido en un <input id="buscar" $gt;
  $('#buscar').change(function(){
    let value = $(this).val()
    console.log(value)
    
    $.ajax({
      type : 'GET',
      ...
    )}
  );
});

// Click en el botón
function Pulso(value) {
  // Para poner otra vez funciones jQuery en el DOM actual
  $(function () {
    console.log(value)
    ...

  });

}
```

Al recibir los datos del AJAX, tendremos que formar un html con las filas de la tabla y ponerlo en su sitio

```javascript
    let htmlString = ''
    $.each(data, function (i, v) {
      htmlString += `<tr id="${v.id}"><td> ... </td> ... 
      <td><button onclick="Pulso('${v.id}')">Borrar</button></td> </tr>` 
    })
    // Poner htmlString en su lugar de la página
  
```

#### 8.2.3. Referencias:

- [jQuery AJAX](https://uniwebsidad.com/libros/fundamentos-jquery/capitulo-7/metodos-ajax-de-jquery)
- [5 jQuery.each() Function Examples](https://www.sitepoint.com/jquery-each-function-examples/)
- [ES6 Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
- [jQuery **html**](https://api.jquery.com/html/)
- [jQuery **hide**](https://api.jquery.com/hide/)
- [jQuery **addClass**](https://api.jquery.com/addclass/)
- [jQuery **removeClass**](https://api.jquery.com/css/)
- [jQuery **css**](https://api.jquery.com/hide/)

---

## Práctica 9: Despliegue en producción

[Práctica en proceso]

### 9.1. Cómo funciona la aplicación

[Práctica en proceso]

### 9.2. Descripción de tareas de la práctica 9

#### 9.2.1. Resumen

El último paso de una aplicación web consiste en desplegarla en un ambiente de 'producción', es decir funcionando con la depuración en 'OFF' y conectada a un servidor web como [ngix](https://www.nginx.com/) o [apache](http://httpd.apache.org/) en el puerto 80.

La aplicación para poner en producción en está última práctica puede ser la que se hizo con flask, o la de django.

#### 9.2.2. Despliegue con docker-compose

Hay que ampliar **docker-compose.yml** para incluir un servicio más: el servidor web `nginx` que se encargará de:

1. Servir los archivos del **static**, que no tienen que pasar por nuestra aplicación.
2. Recibir los requests de los clientes y pasarlos a la aplicación, actuando como [proxy inverso](https://es.wikipedia.org/wiki/Proxy_inverso), y balanceo de carga en su caso
3. Encargarse del cifrado del http, redirigiendo a https cuando sea apropiado



Lo primero es cambiar a la configuración de producción. Esto se hace en django cambiando dos variables en el archivo **settings.py**:

```python
DEBUG = False
ALLOWED_HOSTS = ['*']
```

Con esto dejará de funciona el servidor de desarrollo, y de servir los contenidos de **/static**, que tendrán que pasar a servirse desde el servidor web de producción. Django tiene un script: [collectstatic](https://docs.djangoproject.com/en/3.1/ref/contrib/staticfiles/) para facilitar pasar los contenidos a otro directorio.

También hay que substituir el servidor de desarrollo, `runserver` por el de producción, [gunicorn](http://gunicorn.org/), que hay que instalar con pip. No viene en la instalación de Django.

Quedaría ampliar **docker-compose.yml** para incluir otro servicio, y conectarlos:

![img](https://storage.googleapis.com/zenn-user-upload/qwazyqc1ie3k2d4e7clgltckw7zx)

**docker-compose.yml**:

```yaml
version: '3.7'
       services:
          nginx:
             image: nginx:alpine
             ports:
                - 80:80
             # directorios para el archivo de configuración y archivos del static
             volumes:
                - ./conf:/etc/nginx/conf.d
                - ./web/static:/var/www/static 
             depends_on:
                - web
          web:
             build: .
             restart: always
             command: gunicorn mi_sitio_web.wsgi:application --bind 0.0.0.0:8000
             # command: python manage.py runserver 0.0.0.0:8000
             volumes:
               - ./web:/web

          ...
```

Donde hemos creado una nueva carpeta **conf** para poner la configuración de nginx



El archivo configuración de [nginx](http://nginx.org/en/docs/beginners_guide.html): en  **./conf/default**

```bash
server {
    listen 80 default_server;
 
       # servidor web para archivos en  /static
       location /static/ {
             alias /var/www/static/;
       }
 
       # proxy inverso, se pasa a la aplicación wsgi
       location / {
            proxy_pass http://web:8000;
            proxy_set_header X-Forwarded-Host $server_name;
            proxy_set_header X-Real-IP $remote_addr;
       }
    }
```



Aquí se dan algunos detalles más: [Packaging a Django App Using Docker, NGINX, and Gunicorn](https://www.pluralsight.com/guides/packaging-a-django-app-using-docker-nginx-and-gunicorn), o aquí para flask: [Dockerizing Flask with Postgres, Gunicorn, and Nginx](https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/)

---

## Práctica 10: Javascript

[Práctica en proceso]

### 10.1. Cómo funciona la aplicación

[Práctica en proceso]

### 10.2. Descripción de tareas de la práctica 10

Consistirá en una práctica "libre" de Javascript. La idea es que se integre en vuestra página algo más de funcionalidad en el front-end. Este ejercicio, a diferencia de los anteriores, tiene que estar comentado y documentado para poder ser corregido de manera offline por los profesores (si no, no nos dará tiempo a corregir y tener las actas para la fecha límite). Esta documentación (no hace que sea muy extensa) debe cubrir aspectos como: objetivo (lo que queréis implementar), posibles alternativas (tecnologías o bibliotecas que se pueden usar), problemas que nos hemos encontrado y resolución de los mismos, etc. Por ejemplo, algunas ideas posibles para este ejercicio son:

- Mapas interactivos donde se muestre algo (usando Google Maps o Open Street Maps). Ejemplos: se puede añadir un campo de localización a cada Pokemon de la base de datos (o cada película) y que aparezcan representados en un mapa o calcular rutas en un mapa desde nuestra página web.
- Utilizar algunas bibliotecas de representación de datos estadísticos y gráficas como Highcharts o d3.js para mostrar de manera gráfica algunos datos de nuestra web.
- Utilizar algún framework de frontend específico a nuestra elección (¿react?, ¿vue js?)
- Cualquier otra idea similar.